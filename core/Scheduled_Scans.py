import os
import sys
import threading
from Scan_additional_files import scan
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta, datetime
import json
DAY = 2
WEEK = 1
MONTH = 0

def calc_time(freq):

    freq = int(freq)
    target_data = ''
    _json = ''
    if os.path.exists("./scheduled_params.txt"):
        print("exists")
        with open("./scheduled_params.txt", 'r') as _file:
            _json = _file.read()
        if not _json == "":
            print("OLD JSON:" + _json)
            _data = json.loads(_json)
            target_data = _data["Next_Scan_Date"]
            _freq = _data["Frequency"]
            if freq == _freq:
                freq = _freq
                target_data = target_data.split(" ")[0]
                target_data = datetime.strptime(target_data, '%Y-%m-%d')

                t = date.today().strftime('%Y-%m-%d')
                date_today = datetime.strptime(t, '%Y-%m-%d')
                if target_data < date_today:
                    print("Updated time")
                    target_data += timedelta(days=freq)
            else:
                target_data = create_data_scan(freq)
        else:
            target_data = create_data_scan(freq)
    if not os.path.exists("./scheduled_params.txt"):
        print("not exists")
        target_data = create_data_scan(freq)

    save_next_scan(str(target_data), freq)
    return str(target_data)


def create_data_scan(freq):
    target_data = ''
    freq = int(freq)

    if freq == DAY:
        target_data = date.today() + timedelta(days=1)
    if freq == WEEK:
        target_data = date.today() + timedelta(days=7)
    if freq == MONTH:
        target_data = date.today() + relativedelta(months=+1)

    return str(target_data)


def save_next_scan(target_data, freq):
    scan_params = {"Frequency": freq, "Next_Scan_Date": target_data}
    print("NEW JSON:", scan_params)
    with open("./scheduled_params.txt", 'w') as _file:
        _file.write(json.dumps(scan_params))


def monitor_time(freq, path):
    target_data = calc_time(freq)
    today = date.today()
    target_data = target_data.split(" ")[0]
    print(target_data)
    
    while True:

        if target_data == (today.strftime('%Y-%m-%d')):
            print("Starting Scan")
            scan(path, 'signature')
    


def start(freq, path):
    t1 = threading.Thread(target=monitor_time(freq, path))
    t1.start()


def main():
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    print("freq: " + arg1)
    print("path: " + arg2)
    start(arg1, arg2)
    #start(2, "C:\\daniel")


if __name__ == '__main__':
    main()

