import dns.resolver

zen_return_codes = {
    '127.0.1.2': 'Spam domain',
    '127.0.1.4': 'Phishing domain',
    '127.0.1.5': 'Malware domain',
    '127.0.1.6': 'Botnet C&C domain',
    '127.0.1.102': 'Abused legit spam',
    '127.0.1.103': 'Abused spammed redirector domain',
    '127.0.1.104': 'Abused legit phish',
    '127.0.1.105': 'Abused legit malware',
    '127.0.1.106': 'Abused legit botnet C&C',
    '127.0.1.255': 'IP queries prohibited!',
    '127.0.0.2': 'Static UBE sources, verified spam services',
    '127.0.0.3': 'Static UBE sources, verified spam services',
    '127.0.0.4': 'Illegal 3rd party exploits, including proxies, worms and trojan exploits',
    '127.0.0.5': 'Illegal 3rd party exploits, including proxies, worms and trojan exploits',
    '127.0.0.6': 'Illegal 3rd party exploits, including proxies, worms and trojan exploits',
    '127.0.0.7': 'Illegal 3rd party exploits, including proxies, worms and trojan exploits',
    '127.0.0.10': 'IP ranges which should not be delivering unauthenticated SMTP email',
    '127.0.0.11': 'IP ranges which should not be delivering unauthenticated SMTP email'
}

zen_return_codes_cif = {
    '127.0.1.2': 'Spam',
    '127.0.1.4': 'Phishing',
    '127.0.1.5': 'Malware',
    '127.0.1.6': 'Botnet',
    '127.0.1.102': 'Spam',
    '127.0.1.103': 'Spam',
    '127.0.1.104': 'Phishing',
    '127.0.1.105': 'Malware',
    '127.0.1.106': 'Botnet',
    '127.0.1.255': 'Unknown!',
    '127.0.0.2': 'Spam',
    '127.0.0.3': 'Spam',
    '127.0.0.4': 'Exploit',
    '127.0.0.5': 'Exploit',
    '127.0.0.6': 'Exploit',
    '127.0.0.7': 'Exploit',
    '127.0.0.10': 'Dynamic IP',
    '127.0.0.11': 'Dynamic IP'
}


class AddressChecker:
    query_resp = {
        'status': '0',
        'response_code': '',
        'url': ''
    }

    def check_address(self, ip_address):
        """
        Checks a given ip address in the Spamhaus "ZEN" blacklist.
        :param ip_address: ip address to check
        :type ip_address: str
        :return: json containing the query result with the following properties:
                    status: 0 if clean, 1 if malicious
                    resp: the malicious activity
                    resp_desc: description of activity
        :rtype: list
        """
        resolver = dns.resolver.Resolver()
        all_result_responses = []

        try:
            # Reverse DNS lookup
            domain_name = dns.reversename.from_address(ip_address)
            domain_name = str(domain_name).replace("in-addr.arpa.", "zen.spamhaus.org.")

            # Query the Spamhaus database
            result = resolver.query(domain_name)

            for data in result:
                self.query_resp = {
                    'status': '1',
                    'resp': zen_return_codes[data.address],
                    'assessment': zen_return_codes_cif[data.address]
                }
                all_result_responses.append(self.query_resp)

            return all_result_responses

        except (dns.exception.SyntaxError, dns.resolver.NoNameservers, dns.resolver.NXDOMAIN) as e:
            return [{
                'status': '-1',
                'resp': 'Warning',
                'assessment': e
            }]
