
import time
import os
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from Scan_additional_files import scan


class FileWatcher:
    def __init__(self, src_path):
        self.__src_path = src_path
        self.__event_handler = FileEventHandler()
        self.__event_observer = Observer()

    def run(self):
        self.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self.stop()

    def start(self):
        self.__schedule()
        self.__event_observer.start()

    def stop(self):
        self.__event_observer.stop()
        self.__event_observer.join()

    def __schedule(self):
        self.__event_observer.schedule(
            self.__event_handler,
            self.__src_path,
            recursive=True)


class FileEventHandler(FileSystemEventHandler):
    def __init__(self):
        super().__init__()

    def on_created(self, event):
        self.process(event)

    @staticmethod
    def process(event):
        print("Cacheted file")

        if ".zip" or ".exe" in event.src_path:
            scan(event.src_path, "signature")  # TODO: Change to "behavior"
        scan(event.src_path, "signature")


def main():
    # arg = sys.argv[1]   # Path to real time folder to scan
    watcher = FileWatcher(os.path.join(os.getenv('USERPROFILE'), "Downloads"))
    watcher.run()


if __name__ == '__main__':
    main()
