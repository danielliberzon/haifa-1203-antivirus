import os
import sys

import virtualbox
import time
from virtualbox.library import FileCopyFlag, VBoxErrorIprtError, ProcessCreateFlag
from Signature import check_encrypted_zip
from utils import Logger

TEST_FILE_PATH = "C:\\Users\\sandbox\\Desktop\\my_new_file.txt"
GUEST_DESKTOP_PATH = "C:\\Users\\sandbox\\Desktop\\"
UNZIP_COMMAND = ['C:\\Program Files\\7-Zip\\7z.exe', 'x', GUEST_DESKTOP_PATH + "copied_archive.zip",
                 "-o" + GUEST_DESKTOP_PATH + "copied_archive_unzipped", "-pinfected"]
CPU_USAGE_COMMAND = "wmic cpu get loadpercentage"
TOTAL_RAM = "wmic ComputerSystem get TotalPhysicalMemory"
AVAILABLE_RAM = "wmic OS get FreePhysicalMemory"

POWERED_OFF = 1


class Sandbox:
    def __init__(self, name):
        self.session = None
        self.machine = None
        self.name = name
        self.gs = None

    def start_sandbox(self):
        print("--[ Starting the VM ]--")
        vbox = virtualbox.VirtualBox()
        self.session = virtualbox.Session()
        self.machine = vbox.find_machine(self.name)
        print("--[ Sandbox Started ]--")

    def shut_down(self):
        print("--[ Shutting Down the VM ]--")
        self.gs.close()
        power_down_process = self.session.console.power_down()
        power_down_process.wait_for_completion()
        time.sleep(5)

    def restore_sandbox_snapshot(self):
        self.machine.lock_machine(self.session, virtualbox.library.LockType.shared)
        snap = self.session.machine.find_snapshot("before_testing_ready")
        self.session.machine.restore_snapshot(snap)


class Win7Sandbox(Sandbox):
    def __init__(self, name):
        Sandbox.__init__(self, name)

    def start_traffic_sniffing(self):
        adapter = self.session.machine.get_network_adapter(0)
        adapter.trace_file = os.path.abspath('mycapture.pcap')
        adapter.trace_enabled = True

    def stop_traffic_sniffing(self):
        adapter = self.session.machine.get_network_adapter(0)
        adapter.trace_enabled = False

    def install_program(self, path):
        if not os.path.isfile(path):
            return False

        FileManager.copy_file_host_to_guest(self.gs, path)

        if FileManager.check_file_extension(path) == "zip":
            # Unzip archive to folder
            path_to_unzip = GUEST_DESKTOP_PATH + os.path.basename(path)
            if not check_encrypted_zip(path):
                FileManager.unzip_archive(self.gs, path_to_unzip, "mysubsarethebest")
            else:
                print("--[ Please enter only unencrypted ZIP files! ]--")
                return False
            unzipped_path = GUEST_DESKTOP_PATH + os.path.splitext(os.path.basename(path))[0]

            # Find an executable in the folder
            program_to_execute = FileManager.find_exe_in_dir(self.gs, unzipped_path)
            if not program_to_execute:
                return False
            # Execute the found file
            path_to_execute = GUEST_DESKTOP_PATH + os.path.basename(path) + "\\" + program_to_execute
            FileManager.execute_file(self.gs, path_to_execute)
        elif FileManager.check_file_extension(path) == "exe":
            FileManager.execute_file(self.gs, GUEST_DESKTOP_PATH + os.path.basename(path))
        else:
            print("--[ Please enter only ZIP or EXE files! ]--")
            return False
        return True

    def create_session(self):
        print("--[ Launching VM Process ]--")
        initial_machine_state = self.machine.state

        progress = self.machine.launch_vm_process(self.session, "headless", [])
        progress.wait_for_completion(-1)

        # Wait for the system to load relative to its initial state
        if initial_machine_state == POWERED_OFF:
            time.sleep(15)
        time.sleep(5)

        self.start_traffic_sniffing()
        self.gs = self.session.console.guest.create_session('sandbox', '12345')
        print("--[ Session Created ]--")


class FileManager:
    @staticmethod
    def find_exe_in_dir(gs, dir_path):
        see_file_list_command = "dir " + dir_path + " /B"
        _, stdout, stderr = gs.execute('C:\\Windows\\System32\\cmd.exe', ['/C', see_file_list_command])
        # Go through all files in folder
        for file_name in stdout.decode("utf-8").splitlines():
            if FileManager.check_file_extension(file_name) == 'exe':
                return file_name
        return False

    @staticmethod
    def create_test_file(gs):
        FileManager.make_file(gs, "my_new_file.txt")

    @staticmethod
    def copy_file_host_to_guest(gs, host_file_path):
        # file_name = host_file_path.split("\\")[-1]
        path_of_file_in_guest = GUEST_DESKTOP_PATH + os.path.basename(host_file_path)
        copy_process = gs.file_copy_to_guest(host_file_path, path_of_file_in_guest, [FileCopyFlag(0)])
        copy_process.wait_for_completion()

    @staticmethod
    def check_file_extension(path):
        if not os.path.isfile(path):
            return "dir"
        return path.split('.')[-1]

    @staticmethod
    def make_file(gs, file_name):
        command = (' '.join(['type', 'nul', '>', GUEST_DESKTOP_PATH + file_name]))
        gs.execute('C:\\Windows\\System32\\cmd.exe', ['/C', command])

    @staticmethod
    def unzip_archive(gs, path, password=""):
        unzip_command = ['C:\\Program Files\\7-Zip\\7z.exe', 'x', path, "-o" + os.path.splitext(os.path.basename(path))[0], "-p" + password]
        _, stdout, stderr = gs.execute('C:\\Windows\\System32\\cmd.exe', ['/C'] + unzip_command)

    @staticmethod
    def execute_file(gs, file_path):
        gs.process_create(file_path, [], [], [ProcessCreateFlag(1)], 0)

    @staticmethod
    def check_if_test_file_changed(gs):
        try:
            found = gs.file_exists(TEST_FILE_PATH)
            if found == 1:
                print("File Found")
                return True
            else:
                print("File Not Found, INFECTED!")
                return False
        except VBoxErrorIprtError:
            print("Checking existence ERROR")

    @staticmethod
    def check_system_info(gs):
        cpu = ''
        total_ram = ''
        ava_ram = ''
        process, stdout, stderr = gs.execute('C:\\Windows\\System32\\cmd.exe', ['/C', CPU_USAGE_COMMAND])
        cpu = stdout
        cpu_use = FileManager.decode_str(cpu)
        cpu_use = int(cpu_use)
        #print("CPU: {}%".format(cpu_use))

        process, stdout, stderr = gs.execute('C:\\Windows\\System32\\cmd.exe', ['/C', TOTAL_RAM])
        total = stdout
        total_ram = FileManager.decode_str(total)
        total_ram = int(total_ram)/1000000
        #print("Total RAM: {} mb".format(total_ram))

        process, stdout, stderr = gs.execute('C:\\Windows\\System32\\cmd.exe', ['/C', AVAILABLE_RAM])
        ava = stdout
        ava_ram = FileManager.decode_str(ava)
        ava_ram = int(ava_ram)/1000
        #print("Available RAM: {} mb".format(ava_ram))

        if ava_ram < 1000 or cpu_use > 50:
            print("--[ Infected, Thread Ditected!! ]--")


    @staticmethod
    def decode_str(strr):

        str_decoded = strr.decode()
        str_decoded = str_decoded.replace('\r', '')
        str_decoded = str_decoded.replace('\n', '')
        str_decoded = str_decoded.replace(' ', '')
        if "LoadPercentage" in str_decoded:
            return str_decoded.split('LoadPercentage')[1]
        elif "TotalPhysicalMemory" in str_decoded:
            return str_decoded.split('TotalPhysicalMemory')[1]
        else:
            return str_decoded.split('Memory')[1]


class MalwareAnalyser:
    def __init__(self, win_sandbox):
        """
        Initializes the MalwareAnalyser
        :param win_sandbox:
        :type win_sandbox: Win7Sandbox
        """
        self.win_sandbox = win_sandbox

    def check_for_ransomware(self, file_path_to_check):
        FileManager.create_test_file(self.win_sandbox.gs)
        if self.win_sandbox.install_program(file_path_to_check):
            try:
                print("###START TIMER###")
                time.sleep(70)
                print("###END TIMER###")
                if FileManager.check_if_test_file_changed(self.win_sandbox.gs):
                    print("--[ File is SAFE!!! ]--")
                    return False
                else:
                    print("--[ Threat Found!!! ]--")
                    return True
            except Exception:
                print("Lost Connection to Machine, INFECTED!")
                return True
        else:
            print("ERR: Please Check the Following: \n1. File exist in path \n2. File is EXE or ZIP \n3. IF file is a ZIP, it must contains an EXE file")
            return False


def sandbox_scanner(file_path):
    # Start Sandbox
    session = Win7Sandbox("sandbox_")
    session.start_sandbox()

    # Start Operating System Guest Session
    session.create_session()

    # Initialize Malware Analyser
    malware_analyser = MalwareAnalyser(session)
    if malware_analyser.check_for_ransomware(file_path):
        result = Logger.get_file_detection_summary({"Name": "Win.Ransomware", "file_path": file_path})
    else:
        result = Logger.get_file_detection_summary({"Name": "Clean", "file_path": file_path})

    FileManager.check_system_info(session.gs)

    # Shut down VM
    session.stop_traffic_sniffing()
    session.shut_down()
    session.restore_sandbox_snapshot()

    return result


def main():
    arg = sys.argv[1]
    sandbox_scanner(arg)


if __name__ == '__main__':
    main()
