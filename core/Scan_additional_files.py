
from ast import Try
import glob
import os
import sys
import time

import traceback
import sys

import utils.Logger as Logger
from Signature import signature_scanner
from Sandbox import sandbox_scanner


def get_list_of_files(dir_name):
    print('Getting all files at', dir_name)
    all_files = []
    for rootDir, allSubs, files in os.walk(dir_name):
        if files:
            for file in files:
                all_files.append(rootDir + "\\" + file)
    return all_files

def get_last_edited_file(path):
    list_of_files = glob.glob(path)
    latest_file = max(list_of_files, key=os.path.getctime)
    return latest_file


def scan_by_type(file_path, scan_type):
    return {
        'signature': lambda path: signature_scanner(path),
        'behavior': lambda path: sandbox_scanner(path),
    }[scan_type](file_path)


def scan(path, scan_type):

    print("Scan started:")

    logger = Logger.Logger()
    files = get_list_of_files(path) if os.path.isdir(path) else [path]

    found_malicious = 0
    total_scanned = 0
    t_start = time.time()
    for file in files:
        try:
            print(f"file: {file}")

            result = scan_by_type(file, scan_type)

            if result["Name"] != "Clean":
                logger.inc_infected_scanned()
                found_malicious += 1
            logger.inc_file_scanned()
            total_scanned += 1
            print("RESULT: ", result)
            logger.add_scan_result(Logger.get_file_detection_summary(result))
            print(f"finished_items: {total_scanned}")
            print(f"found_malicious: {found_malicious}")
            print("CLS")
        except PermissionError:
            print("File requires privilege")
            continue

        #except Exception as e:
        #    print("! Scan Interrupted ! -", e)
        #    t_end = time.time()
        #    logger.set_scan_time(t_end - t_start)
        #    logger.apply_json_to_log()
        #    continue

        
    t_end = time.time()
    logger.set_scan_time(t_end - t_start)
    logger.apply_json_to_log()
    print("scan_completed")
    print(f"log_file: {logger.get_log_file_path()}")


def main():
    file_path = sys.argv[1]
    scan_type = sys.argv[2]
    scan(file_path, scan_type)


if __name__ == '__main__':
    main()
