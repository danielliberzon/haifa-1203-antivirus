import base64
import hashlib
from Crypto.Cipher import AES
from Crypto import Random
import json
import pathlib
from Password_Saver import PasswordManager
import sys

DB_FILE = str(pathlib.Path().resolve()) + "\\password_data.db"


Q_FETCH_ALL_DATA = '''
SELECT * FROM data
'''

class PasswordGenerator:


    def password_generator(self, master_password, block_size):

        block_size_key = self.compute_block_sized_key(master_password, block_size).encode('utf_8')
        ipad = b'\x36' * 16
        opad = b'\x5c' * 16
        o_key_pad = self.xor(block_size_key, opad)
        i_key_pad = self.xor(block_size_key, ipad)

        return hashlib.sha1(o_key_pad or i_key_pad)

    @staticmethod
    def bitwise_xor_bytes(a, b):
        result_int = int.from_bytes(a, byteorder="big") ^ int.from_bytes(b, byteorder="big")
        return result_int.to_bytes(max(len(a), len(b)), byteorder="big")

    @staticmethod
    def xor(x, y):
        return bytes(x[i] ^ y[i] for i in range(min(len(x), len(y))))

    @staticmethod
    def compute_block_sized_key(master_password, block_size):

        if len(master_password) > block_size:
            master_password = hashlib.sha1(master_password)

        if len(master_password) < block_size:
            return master_password.zfill(block_size)

        return master_password


class AESCipher(object):

    def __init__(self, key):
        self.bs = AES.block_size
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw.encode()))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


def encrypt_passwords(AES_):

    password_obj = PasswordManager()
    _info = password_obj.get_all_info()
    encrypted_ = []
    for row in _info:
       json.dumps(row).replace('"', '')
    print(_info)
    for i in _info:
        title = AES_.encrypt(i[1]).decode("utf-8")
        url = AES_.encrypt(i[2]).decode("utf-8")
        username = AES_.encrypt(i[3]).decode("utf-8")
        password = AES_.encrypt(i[4]).decode("utf-8")
        obj_ = [i[0], title, url, username, password]
        encrypted_.append(obj_)
    print(encrypted_)

    for ch in encrypted_:
        password_obj.update_data(ch[0], ch[1], ch[2], ch[3], ch[4])


def decrypt_passwords(AES_):

    password_obj = PasswordManager()
    _info = password_obj.get_all_info()
    decrypted_ = []
    for row in _info:
       json.dumps(row).replace('"', '')

    print(_info)

    for i in _info:
        title = AES_.decrypt(i[1])
        url = AES_.decrypt(i[2])
        username = AES_.decrypt(i[3])
        password = AES_.decrypt(i[4])
        obj_ = [i[0], title, url, username, password]
        decrypted_.append(obj_)
    print(decrypted_)

    for ch in decrypted_:
        password_obj.update_data(ch[0], ch[1], ch[2], ch[3], ch[4])
    print("Decrypted")

def start(user_input, encrypt_decrypt, is_first_time, close):

    password_g = PasswordGenerator()
    password = password_g.password_generator(user_input, 16).hexdigest()
    pj = PasswordManager()
    AES_ = AESCipher(password)
    if is_first_time == "1":
        AES_ = AESCipher(password)
        encrypt_passwords(AES_)
    elif close == "1":
        encrypt_passwords(AES_)
    else:
        correct = str(pj.get_first()[4])
        decrypted = AES_.decrypt(correct)
        if user_input == decrypted:
            if encrypt_decrypt == "1":
                encrypt_passwords(AES_)
            else:
                decrypt_passwords(AES_)
                pj.fetch_all_accounts_data()

        else:
            print("-i")

