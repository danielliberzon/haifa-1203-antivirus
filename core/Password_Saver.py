import json
import pathlib
import sqlite3
import sys
import Password_Transcriptor

DB_FILE = str(pathlib.Path().resolve()) + "\\password_data.db"

Q_CREATE_TABLE = '''
CREATE TABLE IF NOT EXISTS data (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    TITLE TEXT NOT NULL,
    URL TEXT NOT NULL,
    USERNAME text NOT NULL,
    PASSWORD text NOT NULL,
    UNIQUE(TITLE));
'''

Q_INSERT_PASSWORD_DATA = '''
INSERT INTO data(TITLE, URL, USERNAME, PASSWORD) VALUES ('{0}', '{1}', '{2}', '{3}')
'''

Q_UPDATE_PASSWORD_DATA = '''
UPDATE data SET URL = '{1}', USERNAME = '{2}', PASSWORD = '{3}' WHERE TITLE = '{0}'
'''

Q_UPDATE_PASSWORD_DATA_ID = '''
UPDATE data SET TITLE = '{1}', URL = '{2}', USERNAME = '{3}', PASSWORD = '{4}' WHERE ID = '{0}'
'''

Q_FETCH_ALL_DATA = '''
SELECT * FROM data
'''

Q_DELETE_PASSWORD_DATA = '''
DELETE FROM data WHERE TITLE = '{0}'
'''

WORKING_DIR = str(pathlib.Path().resolve())


class PasswordManager:
    def __init__(self):
        self.connection = sqlite3.connect(DB_FILE)
        self.create_database()

    def add_account(self, title, url, username, password):
        
        self.connection.execute(Q_INSERT_PASSWORD_DATA.format(title, url, username, password))

        self.connection.commit()
        print("-aDONE")

    def update_account(self, title, url, username, password):
        self.connection.execute(Q_UPDATE_PASSWORD_DATA.format(title, url, username, password))
        self.connection.commit()

    def update_data(self, _id, title, url, username, password):
        self.connection.execute(Q_UPDATE_PASSWORD_DATA_ID.format(_id, title, url, username, password))
        self.connection.commit()

    def get_first(self):

        cursor = self.connection.cursor()
        cursor.execute(Q_FETCH_ALL_DATA)
        result = cursor.fetchall()

        return result[0]

    def get_all_info(self):

        cursor = self.connection.cursor()
        cursor.execute(Q_FETCH_ALL_DATA)
        result = cursor.fetchall()

        return result

    def fetch_all_accounts_data(self):

        result = self.get_all_info()
        try:
            if "PasswordTester" in result[0][1]:
                result.pop(0)
                for row in result:
                    print("-f"+json.dumps(row).replace('"', ''))
        except:
            print(result)


    def delete_account(self, title):
        self.connection.execute(Q_DELETE_PASSWORD_DATA.format(title))
        self.connection.commit()
        print("-dDONE")

    def create_database(self):
        self.connection.execute(Q_CREATE_TABLE)


def parse_request(command_args):
    transcriptor = Password_Transcriptor
    manager = PasswordManager()
    print({
        '-a': lambda args: manager.add_account(*args),
        '-u': lambda args: manager.update_account(*args),
        '-all': lambda args: manager.fetch_all_accounts_data(),
        '-d': lambda args: manager.delete_account(args[0]),
        '-ed': lambda args: transcriptor.start(*args),
    }[command_args[1]](command_args[2:]))
    manager.connection.close()


def main():
    print(DB_FILE)
    print("Requested:", sys.argv)
    try:
        parse_request(sys.argv)
    except TypeError as e:
        print(e)
    except sqlite3.IntegrityError:
        print("Account title already exists")
    except sqlite3.Error as er:
        print('SQLite error: %s' % (' '.join(er.args)))
        print("Exception class is: ", er.__class__)


if __name__ == '__main__':
    main()
