import os

from pymongo import MongoClient
import hashlib
from utils import Logger
from zipfile import ZipFile

import pathlib

WORKING_DIR = str(pathlib.Path().resolve())
BLOOM_ARR_SIZE = 5000000


def check_encrypted_zip(file_path):
    if ".zip" in file_path:
        with ZipFile(file_path, 'r') as f:
            for zip_info in f.infolist():
                has_password = zip_info.flag_bits & 0x1
                if has_password:
                    return True
    return False


def get_signature(file_path):
    print("--- Getting Signature ---")
    if ".zip" in file_path:
        with ZipFile(file_path, 'r') as f:
            return f.read(f.namelist()[0])
    else:
        with open(file_path, 'rb') as f:
            return f.read()


def check_signature(b_bytes, filter):
    print("--- Checking Signature Hashes In Filter ---")
    return check(b_bytes, 1, filter)


def check(sig, hash_method, filter):
    if hash_method == 1:
        res = int(hashlib.md5(sig).hexdigest(), 16)
        place = res % BLOOM_ARR_SIZE
        if filter.check_if_may_inside(place):
            return check(sig, 2, filter)

    elif hash_method == 2:
        res = int(hashlib.sha1(sig).hexdigest(), 16)
        place = res % BLOOM_ARR_SIZE
        if filter.check_if_may_inside(place):
            return True
    return False


class DatabaseManager:
    def __init__(self, database_name, collection_name):
        client = MongoClient('localhost:27017')
        self.__col = client[database_name][collection_name]

    def check_infected_file(self, file_content):

        md5_sig = hashlib.md5(file_content).hexdigest()
        return self.__col.find_one({'Signatures': {'$regex': md5_sig}})

    def get_all_signatures(self):
        print("### Getting Virus Signatures ###")
        data_set = self.__col.find({})
        signatures = []
        for x in data_set:
            signatures.append(x['Signatures'])

        return signatures


class BloomFilter:

    def __init__(self, db_manager):
        self.__db_manager = db_manager
        self.__bloom_array = [0] * BLOOM_ARR_SIZE

    def create_bloom_array(self):
        path = os.path.dirname(os.path.realpath(__file__)) + "\\utils\\bloom_filter.txt"
        if os.path.exists(path):
            self.load_bloom_filter(path)
        else:
            print("### CREATING BLOOM FILTER ###")
            for sig in self.__db_manager.get_all_signatures():
                hash_value = int(sig, 16)
                place_in_bloom_array = hash_value % BLOOM_ARR_SIZE
                self.__bloom_array[place_in_bloom_array] = "1"
            self.save_bloom_filter(path)

    def check_if_may_inside(self, place_in_bloom_array):
        if self.__bloom_array[place_in_bloom_array] == " 1":
            return True
        return False

    def print_array(self):
        print(self.__bloom_array)

    def save_bloom_filter(self, path):

        with open(path, 'w') as bloom_file:
            bloom_file.write(str(self.__bloom_array))
            bloom_file.close()
        print("### BLOOM FILTER SAVED ###")

    def load_bloom_filter(self, path):

        print("### Loading Bloom Filter ###")
        with open(path, 'r') as bloom_file:
            self.__bloom_array = list(bloom_file.read().split(','))
            bloom_file.close()


def signature_scanner(file_path):
    if check_encrypted_zip(file_path):
        print("Password Encrypted .zip Inputted. Skipping.")
        return 0

    db_manager = DatabaseManager("VirusDB", "viruses")

    print("### Generating Bloom Filter ###")
    _filter = BloomFilter(db_manager)
    _filter.create_bloom_array()

    print("### Scanning Started ###")
    file_path_to_scan = file_path
    file_content_to_scan = get_signature(file_path_to_scan)
    res = check_signature(file_content_to_scan, _filter)
    if res:
        print("### Potential For Virus - Checking Database ###")
        res = db_manager.check_infected_file(file_content_to_scan)
        if res:
            res["file_path"] = file_path_to_scan
            print("Found Threat! Press 'View Logs' for more details")
            return Logger.get_file_detection_summary(res)

    print("CLEAN, No Threats were Found!")
    return Logger.get_file_detection_summary({"Name": "Clean", "file_path": file_path_to_scan})

