import dpkt
import socket


def get_ip_set_from_file(path):
    ip_set = set()
    for _, packet in dpkt.pcap.Reader(open(path, 'rb')):
        try:
            eth = dpkt.ethernet.Ethernet(packet)

            # Check if packet has IP data
            if not isinstance(eth.data, dpkt.ip.IP):
                continue

            src_ip = socket.inet_ntop(socket.AF_INET, eth.data.src)
            dest_ip = socket.inet_ntop(socket.AF_INET, eth.data.dst)

            ip_set.add(src_ip)
            ip_set.add(dest_ip)
        except (dpkt.dpkt.NeedData, dpkt.dpkt.UnpackError):
            print('Err: Truncated packet')

    return ip_set
