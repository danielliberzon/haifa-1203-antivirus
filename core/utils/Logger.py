import time
from datetime import datetime
import pathlib
import json
WORKING_DIR = str(pathlib.Path().resolve())


class Logger:
    def __init__(self):
        pathlib.Path(WORKING_DIR + r"\LOGS").mkdir(exist_ok=True)
        self.__log_file_path = WORKING_DIR + r"\LOGS\log_" + datetime.now().strftime("%d-%m-%Y_%H-%M-%S") + ".json"
        self.__log_json = {"scan_time": "",
                           "amount_of_files_scanned": 0,
                           "amount_of_infected_files": 0,
                           "full_results": []}

    def set_scan_time(self, seconds):
        self.__log_json["scan_time"] = time.strftime('%H:%M:%S', time.gmtime(seconds))

    def add_scan_result(self, scan_result):
        self.__log_json["full_results"].append(scan_result)

    def get_log_file_path(self):
        return self.__log_file_path

    def inc_file_scanned(self):
        self.__log_json["amount_of_files_scanned"] += 1

    def inc_infected_scanned(self):
        self.__log_json["amount_of_infected_files"] += 1

    def apply_json_to_log(self):
        with open(self.__log_file_path, 'w') as log_file:
            log_file.write(json.dumps(self.__log_json))


def get_file_detection_summary(scan_res):
    print("SCAN_RES:", scan_res)
    return {
        "date": datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
        "Name": scan_res["Name"],
        "file_path": scan_res["file_path"],
        "type": get_virus_type(scan_res["Name"]),
        "description": get_virus_details(get_virus_type(scan_res["Name"]))
    }


def get_virus_type(virus_name):
    if virus_name == "Clean":
        return "Clean"
    return virus_name.split('.')[1]


def get_virus_details(virus_name):
    try:
        result = {
            'Ransomware': "Ransomware is a type of malware that threatens to publish the victim's personal data or perpetually block access to it unless a ransom is paid.",
            'Trojan': "Trojan is a type of malicious code or software that looks legitimate but can take control of your computer. A Trojan is designed to damage, disrupt, steal, or in general inflict some other harmful action on your data or network. ",
            'Spyware': "Spyware is software with malicious behavior that aims to gather information about a person or organization and send it to another entity in a way that harms the user.",
            'Worm': "A computer worm is a standalone malware computer program that replicates itself in order to spread to other computers. It often uses a computer network to spread itself, relying on security failures on the target computer to access it.",
            'Dropper': "A Trojan dropper is a malicious program designed to deliver other malware to a victim's computer or phone",
            'Cryptojackings': 'Cryptojacking is the unauthorized use of someone else’s computer to mine cryptocurrency',
            'Downloader': "trojan-downloader is a type of trojan that installs itself to the system and waits until an Internet connection becomes available to connect to a remote server or website in order to download additional programs (usually malware) onto the infected computer.",
            'Clean': "The file is clean"
        }[virus_name]
    except:
        return 'This file may harm your computer and your personal data'
    return result
