import pcap_parser
import ip_reputation_checker
import Sandbox

Sandbox.sandbox_scanner("C:\\WannaCrypt0r.zip")
ip_set = pcap_parser.get_ip_set_from_file("mycapture.pcap")
checker = ip_reputation_checker.AddressChecker()

for ip in ip_set:
    result = checker.check_address(ip)
    print(result)
