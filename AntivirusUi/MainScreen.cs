﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntivirusUi
{
    public partial class Antivirus : Form
    {
        private const string signature_scan = "..\\..\\..\\core\\Scan_additional_files.py";
        private const string sandbox_scan = "..\\..\\..\\core\\Sandbox.py";
        private const string scheduled_scan = "..\\..\\..\\core\\Scheduled_Scans.py";
        private string[] pythonPaths = {
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\Programs\\Python\\Python37\\python.exe",
            "C:\\Python37\\python.exe"
        };
        private string pathToScan;
        private int freq;
        private Thread check;
        private Process runningScanProcess;

        private RealTimeScanner realTimeScanner;

        private bool exited;
        private enum scanFreqs
        {
            Monthly,
            Weekly,
            Daily,
            None
        }

        public Antivirus()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            realTimeScanner = new RealTimeScanner();

            freq = Properties.Settings.Default.scheduledFreq;
            pathToScan = Properties.Settings.Default.folderPathToScan;
            Console.WriteLine(Properties.Settings.Default.folderPathToScan);

            if (Properties.Settings.Default.folderPathToScan != "Choose Folder..")
            {

                CheckSettings();
                StartSettingsSync();
            }
        }

        /// <summary>
        /// Checks the current settings that the user has applied 
        /// </summary>
        private void CheckSettings()
        {
            if (Properties.Settings.Default.folderPathToScan != "Choose Folder.." && freq != (int)scanFreqs.None)
            {
                RunScript(scheduled_scan);
            }

        }

        /// <summary>
        /// Starts a thread that syncs the program with newly changed settings.
        /// </summary>
        private void StartSettingsSync()
        {
            check = new Thread(() => Check_if_Changed_Params());
            check.SetApartmentState(ApartmentState.STA);
            check.Start();
        }
        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            if(runningScanProcess != null)
            {
                if (!runningScanProcess.HasExited)
                    runningScanProcess.Kill();
                check.Abort();
            }


            if (runningScanProcess != null && !runningScanProcess.HasExited)
            {
                runningScanProcess.Kill(); 
            }
            exited = true;

        }

        private void Check_if_Changed_Params()
        {
            while (!exited)
            {
                //Console.WriteLine(Properties.Settings.Default.folderPathToScan);
                if (Properties.Settings.Default.folderPathToScan != "")
                {
                    if (freq != Properties.Settings.Default.scheduledFreq || pathToScan != Properties.Settings.Default.folderPathToScan)
                    {
                        if (runningScanProcess != null && !runningScanProcess.HasExited)
                        {
                            runningScanProcess.Kill();
                            Console.WriteLine("-- Scheduled Scanner Stopped --");
                        }
                        freq = Properties.Settings.Default.scheduledFreq;
                        pathToScan = Properties.Settings.Default.folderPathToScan;
                        if (freq != (int)scanFreqs.None)
                        {
                            RunScript(scheduled_scan);
                        }
                    }
                }
                Thread.Sleep(3000);
            }
        }

        private void RunScript(string script_path)
        {
            runningScanProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = File.Exists(pythonPaths[0]) ? pythonPaths[0] : pythonPaths[1],
                    Arguments = string.Format("-u {0} {1} {2}", script_path, freq, pathToScan),
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                },
                EnableRaisingEvents = true
            };

            runningScanProcess.OutputDataReceived += OnProcessOutput;
            runningScanProcess.ErrorDataReceived += RunningScanProcess_ErrorDataReceived;

            runningScanProcess.Exited += RunningScanProcess_Exited;
            runningScanProcess.Start();
            runningScanProcess.BeginOutputReadLine();
            runningScanProcess.BeginErrorReadLine();
            Console.WriteLine("-- Scheduled Scanner Started --");
        }

        private void RunningScanProcess_Exited(object sender, EventArgs e)
        {

        }
        private void RunningScanProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("LOG: " + e.Data);
        }

        private void OnProcessOutput(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("LOG: " + e.Data);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.tvYourName.Text = Properties.Settings.Default.Username;
            this.tvYourName.Width = this.tvYourName.Text.Length * 8;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Sandbox_Click(object sender, EventArgs e)
        {
            SignatureScanScreen sandboxScanScreen = new SignatureScanScreen(true, false);
            sandboxScanScreen.ShowDialog();
        }

        private void Signature_Click(object sender, EventArgs e)
        {
            SignatureScanScreen signatureScanScreen = new SignatureScanScreen(false, false);
            signatureScanScreen.ShowDialog();
        }

        private void Password_Click(object sender, EventArgs e)
        {
            PasswordManagerScreen passwordManagerScreen = new PasswordManagerScreen();
            passwordManagerScreen.ShowDialog();
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            SettingsScreen signatureScanScreen = new SettingsScreen();
            signatureScanScreen.ShowDialog();
        }

        private void FullScan_Click(object sender, EventArgs e)
        {
            SignatureScanScreen signatureScanScreen = new SignatureScanScreen(false, true);
            signatureScanScreen.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
