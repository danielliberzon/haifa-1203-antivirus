﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntivirusUi
{
    public partial class LogScreen : Form
    {
        private JObject jsonLog;
        public LogScreen(string logPath)
        {
            InitializeComponent();
            ReadLogFile(logPath);

            SetLogPathText(logPath);
            tvScanTime.Text = "Scan Time: " + jsonLog["scan_time"];
            tvNumFilesScanned.Text = "Amount of Files Scanned: " + jsonLog["amount_of_files_scanned"];
            tvNumOfInfected.Text = "Amount of Infected Files: " + jsonLog["amount_of_infected_files"];

            SetDataInGrid();
        }

        private void SetLogPathText(string path)
        {
            if (path.Length > 80)
            {
                tvLogPath.Text = "Log Path: " + path.Substring(0, 80) + "...";
            }
            else
            {
                tvLogPath.Text = "Log Path: " + path;
            }
        }

        private void ReadLogFile(string logPath)
        {
            string content = System.IO.File.ReadAllText(logPath);

            this.jsonLog = JObject.Parse(content);
            Console.WriteLine(this.jsonLog["full_results"].GetType());
        }

        private void SetDataInGrid()
        {
            DataTable dataTable = new DataTable();
            DataColumn[] columns = { 
                new DataColumn("Path"), 
                new DataColumn("Threat Name"),
                new DataColumn("Type"),
                new DataColumn("Description"),
            };

            dataTable.Columns.AddRange(columns);

            foreach (JObject res in this.jsonLog["full_results"])
            {
                var row = dataTable.NewRow();
                row["Path"] = res["file_path"];
                row["Threat Name"] = res["Name"];
                row["Type"] = res["type"];
                row["Description"] = res["description"];

                dataTable.Rows.Add(row);
            }

            dgResults.DataSource = dataTable;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
