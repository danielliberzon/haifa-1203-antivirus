﻿
namespace AntivirusUi
{
    partial class LogScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.tvLogPath = new System.Windows.Forms.Label();
            this.tvScanTime = new System.Windows.Forms.Label();
            this.tvNumFilesScanned = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tvNumOfInfected = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgResults = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgResults)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(197, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Scan Results Log";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // tvLogPath
            // 
            this.tvLogPath.AutoSize = true;
            this.tvLogPath.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvLogPath.ForeColor = System.Drawing.Color.White;
            this.tvLogPath.Location = new System.Drawing.Point(21, 96);
            this.tvLogPath.Name = "tvLogPath";
            this.tvLogPath.Size = new System.Drawing.Size(69, 16);
            this.tvLogPath.TabIndex = 1;
            this.tvLogPath.Text = "Log Path: ";
            this.tvLogPath.Click += new System.EventHandler(this.label2_Click);
            // 
            // tvScanTime
            // 
            this.tvScanTime.AutoSize = true;
            this.tvScanTime.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvScanTime.ForeColor = System.Drawing.Color.White;
            this.tvScanTime.Location = new System.Drawing.Point(21, 139);
            this.tvScanTime.Name = "tvScanTime";
            this.tvScanTime.Size = new System.Drawing.Size(71, 16);
            this.tvScanTime.TabIndex = 2;
            this.tvScanTime.Text = "Scan time:";
            // 
            // tvNumFilesScanned
            // 
            this.tvNumFilesScanned.AutoSize = true;
            this.tvNumFilesScanned.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvNumFilesScanned.ForeColor = System.Drawing.Color.White;
            this.tvNumFilesScanned.Location = new System.Drawing.Point(21, 178);
            this.tvNumFilesScanned.Name = "tvNumFilesScanned";
            this.tvNumFilesScanned.Size = new System.Drawing.Size(162, 16);
            this.tvNumFilesScanned.TabIndex = 3;
            this.tvNumFilesScanned.Text = "Amount of Files Scanned:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(254, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "Summary";
            // 
            // tvNumOfInfected
            // 
            this.tvNumOfInfected.AutoSize = true;
            this.tvNumOfInfected.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvNumOfInfected.ForeColor = System.Drawing.Color.White;
            this.tvNumOfInfected.Location = new System.Drawing.Point(21, 271);
            this.tvNumOfInfected.Name = "tvNumOfInfected";
            this.tvNumOfInfected.Size = new System.Drawing.Size(160, 16);
            this.tvNumOfInfected.TabIndex = 5;
            this.tvNumOfInfected.Text = "Amount of Infected Files:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(21, 300);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Detailed List";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // dgResults
            // 
            this.dgResults.AllowUserToAddRows = false;
            this.dgResults.AllowUserToDeleteRows = false;
            this.dgResults.AllowUserToResizeRows = false;
            this.dgResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgResults.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(40)))));
            this.dgResults.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgResults.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgResults.EnableHeadersVisualStyles = false;
            this.dgResults.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            this.dgResults.Location = new System.Drawing.Point(24, 327);
            this.dgResults.Name = "dgResults";
            this.dgResults.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgResults.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgResults.RowHeadersVisible = false;
            this.dgResults.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgResults.Size = new System.Drawing.Size(582, 232);
            this.dgResults.TabIndex = 7;
            this.dgResults.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // LogScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(633, 619);
            this.Controls.Add(this.dgResults);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tvNumOfInfected);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tvNumFilesScanned);
            this.Controls.Add(this.tvScanTime);
            this.Controls.Add(this.tvLogPath);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LogScreen";
            this.Text = "LogScreen";
            ((System.ComponentModel.ISupportInitialize)(this.dgResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label tvLogPath;
        private System.Windows.Forms.Label tvScanTime;
        private System.Windows.Forms.Label tvNumFilesScanned;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label tvNumOfInfected;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgResults;
    }
}