﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntivirusUi
{
    public partial class ListItem : UserControl
    {
        private string accountTitle, url;
        private string siteLogo;
        public ListItem()
        {
            InitializeComponent();
        }

        #region Properties
        [Category("Custom Props")]
        public string Url { 
            get => url; 
            set { url = value; tvUrl.Text = value; } 
        }

        [Category("Custom Props")]
        public string SiteLogo { 
            get => siteLogo; 
            set { siteLogo = value; SetAccountLogo(value); }
        }

        private void SetAccountLogo(string path)
        {
            Console.WriteLine("Loading.. " + path);
            try
            {
                ivLogo.Load(path);
            }
            catch (System.IO.FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                ivLogo.Load("");
            }   
        }

        private void pBg_MouseLeave(object sender, EventArgs e)
        {
            //pBg.BackColor = Color.FromArgb(30, 30, 30);
            //tvAccountTitle.BackColor = Color.FromArgb(30, 30, 30);
            //tvUrl.BackColor = Color.FromArgb(30, 30, 30);
        }

        [Category("Custom Props")]
        public string AccountTitle {
            get => accountTitle; 
            set { accountTitle = value; tvAccountTitle.Text = value; }
        }
        #endregion
    }
}
