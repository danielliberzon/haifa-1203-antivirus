﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace AntivirusUi
{
    public partial class SignatureScanScreen : Form
    {
        private const string signature_scan = "..\\..\\..\\core\\Scan_additional_files.py";
        private const string sandbox_scan = "..\\..\\..\\core\\Sandbox.py";
        private string[] pythonPaths = { 
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\Programs\\Python\\Python37\\python.exe",
            "C:\\Python37\\python.exe" 
        };
        private string pathToScan, scanResultLogPath, scanType;
        private bool finished, sandbox, stopped;
        private Process runningScanProcess;
        public SignatureScanScreen(bool is_sandbox, bool is_full)
        {
            InitializeComponent();
            IvScanning.Enabled = false;
            btnShowLogs.Enabled = false;
            btnShowLogs.ForeColor = Color.Black;
            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            this.scanType = "signature";
            if (is_sandbox)
            {
                this.scanType = "behavior";
                btnChooseFolder.Visible = false;
                btnChooseFolder.Enabled = false;
            }
            else
            {
                if (is_full)
                {
                    this.scanType = "signature";
                    btnChooseFolder.Visible = false;
                    btnChooseFolder.Enabled = false;
                    btnChooseFile.Visible = false;
                    btnChooseFolder.Enabled = false;
                    tvCurrentFile.Text = "C:\\";
                    pathToScan = "C:\\";

                }
            }
        }

        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (runningScanProcess != null)
                    if (!runningScanProcess.HasExited)
                        runningScanProcess.Kill();
            }
            catch (Exception)
            {


            }
        }

        private void ChooseElementToScan(object sender, EventArgs e)
        {
            CommonOpenFileDialog openFileDialog = new CommonOpenFileDialog();
            string path = "";

            openFileDialog.InitialDirectory = "C:\\";
            openFileDialog.IsFolderPicker = ((Button)sender).Name == "btnChooseFolder";
            if (openFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                path = openFileDialog.FileName;
                btnChooseFile.Text = new DirectoryInfo(path).Name;
                setChooseElementButtons(openFileDialog.IsFolderPicker, path);
                pathToScan = path;
            }
            tvCurrentFile.Text = pathToScan;
        }

        private void setChooseElementButtons(bool userChoosesFolder, string chosenPath)
        {
            if (userChoosesFolder)
            {
                btnChooseFile.Text = "Choose File to Scan..";
                btnChooseFolder.Text = new DirectoryInfo(chosenPath).Name;
            }
            else
            {
                btnChooseFile.Text = new DirectoryInfo(chosenPath).Name;
                btnChooseFolder.Text = "Choose Folder to Scan..";
            }

        }

        private void StartScan(object sender, EventArgs e)
        {
            if (pathToScan == null)
            {
                tvCurrentFile.Text = "PLEASE ENTER A FILE TO SCAN!";
                tvCurrentFile.ForeColor = Color.Red;
            }
            else
            {
                btnChooseFile.Enabled = false;
                btnChooseFolder.Enabled = false;
                btnChooseFile.ForeColor = Color.Black;
                btnChooseFolder.ForeColor = Color.Black;
                btnStart.Text = "Stop";
                tvCurrentFile.ForeColor = Color.White;
                tvHeadLine.Text = "Scanning...";
                IvScanning.Enabled = true;
                finished = false;
                string path_to_script = "";

                if (sandbox)
                {
                    path_to_script = sandbox_scan;
                }
                else
                {
                    path_to_script = signature_scan;
                }
                Console.WriteLine("RUN SCRIPT");

                RunScript(path_to_script);
                Thread start_timer = new Thread(() => TimeScanScript());

                start_timer.SetApartmentState(ApartmentState.STA);
                start_timer.Start();

                btnStart.Click -= StartScan;
                btnStart.Click += btnStop_Click;
            }

        }

        private void TimeScanScript()
        {
            int elapsedSeconds = 0;
            string elapsed_time = "";
            while (!finished)
            {
                if (!stopped)
                {
                    Thread.Sleep(1000);
                    elapsedSeconds += 1;
                    elapsed_time = "00:00:00";

                    if (IsHandleCreated)
                    {
                        this.Invoke(new Action(() =>
                        {
                            elapsed_time = $"{(elapsedSeconds / 3600):00}" + ":" + $"{((elapsedSeconds / 60) % 60):00}" + ":" + $"{(elapsedSeconds % 60):00}";
                            tvCurrentElapsedTime.Text = elapsed_time;
                        }));
                    }

                }
            }
            if (!runningScanProcess.HasExited)
                runningScanProcess.Kill();
        }

        private void RunScript(string script_path)
        {
            runningScanProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = File.Exists(pythonPaths[0]) ? pythonPaths[0] : pythonPaths[1],
                    Arguments = string.Format("-u {0} {1} {2}", script_path, pathToScan, scanType),
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                },
                EnableRaisingEvents = true
            };

            runningScanProcess.OutputDataReceived += OnProcessOutput;
            runningScanProcess.ErrorDataReceived += RunningScanProcess_ErrorDataReceived;

            runningScanProcess.Exited += RunningScanProcess_Exited;
            runningScanProcess.Start();
            runningScanProcess.BeginOutputReadLine();
            runningScanProcess.BeginErrorReadLine();
            Console.WriteLine("Process Started");
        }

        private void RunningScanProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("ERROR: " + e.Data);   
        }

        private void RunningScanProcess_Exited(object sender, EventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                btnStart.Text = "Close";
                btnStart.Click += BtnClose_Click;

                if (!stopped)
                {
                    btnShowLogs.Enabled = true;
                    btnShowLogs.ForeColor = Color.Crimson;
                }
                if (int.Parse(tvCurrentFoundMalicious.Text) > 0)
                {
                    tvHeadLine.Text = "Threats Found!";
                    IvScanning.Image = Properties.Resources.high_importance;
                    tvHeadLine.ForeColor = Color.FromArgb(232, 41, 56);
                    btnStart.ForeColor = Color.FromArgb(232, 41, 56);
                }
                else
                {
                    tvHeadLine.Text = "Scan Completed!";
                    IvScanning.Image = Properties.Resources.scan_ended_icon;
                    tvHeadLine.ForeColor = Color.FromArgb(32, 172, 99);
                    btnStart.ForeColor = Color.FromArgb(32, 172, 99);
                }

                btnChooseFile.ForeColor = Color.Black;
                btnChooseFolder.ForeColor = Color.Black;
                
            }));
        }

        private void OnProcessOutput(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                this.Invoke(new Action(() =>
                {
                    LogScanOutput(e.Data);
                }));
            }     
        }

        private void LogScanOutput(string log)
        {
            if (log == "CLS")
            {
                tvProcess.Text = "Scanning " + pathToScan + " ..\n";
            }
            else
            {
                Console.WriteLine(log);
                if (log.StartsWith("file: "))
                {
                    tvCurrentFile.Text = log.Replace("file: ", "");
                }
                if (log.StartsWith("finished_items: "))
                {
                    tvCurrentFinishedItems.Text = log.Replace("finished_items: ", "");
                }
                if (log.StartsWith("found_malicious: "))
                {
                    tvCurrentFoundMalicious.Text = log.Replace("found_malicious: ", "");
                }
                if (log.StartsWith("scan_completed"))
                {
                    finished = true;
                }
                if (log.StartsWith("log_file: "))
                {
                    this.scanResultLogPath = log.Replace("log_file: ", "");
                }
                tvProcess.Text += log;
                tvProcess.Text += '\n';
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            btnStart.Text = "Start";
            IvScanning.Enabled = false;
        }

        private void ShowLogs(object sender, EventArgs e)
        {
            LogScreen logScreen = new LogScreen(this.scanResultLogPath);
            logScreen.ShowDialog();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (!runningScanProcess.HasExited)
            {
                runningScanProcess.Kill();
            }

            finished = true;
            IvScanning.Enabled = false;
            stopped = true;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
