﻿namespace AntivirusUi
{
    partial class Antivirus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tvYourName = new System.Windows.Forms.Label();
            this.btnPassword = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnFullScan = new System.Windows.Forms.Button();
            this.btnSignature = new System.Windows.Forms.Button();
            this.btnSandbox = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Montserrat", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label1.Location = new System.Drawing.Point(161, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(574, 44);
            this.label1.TabIndex = 2;
            this.label1.Text = "YOUR COMPUTER IS PROTECTED";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label2.Location = new System.Drawing.Point(166, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "No issues detected";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tvYourName
            // 
            this.tvYourName.Location = new System.Drawing.Point(12, 516);
            this.tvYourName.Name = "tvYourName";
            this.tvYourName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tvYourName.Size = new System.Drawing.Size(62, 13);
            this.tvYourName.TabIndex = 10;
            this.tvYourName.Text = "Your Name";
            this.tvYourName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPassword
            // 
            this.btnPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            this.btnPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPassword.FlatAppearance.BorderSize = 0;
            this.btnPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPassword.ForeColor = System.Drawing.Color.White;
            this.btnPassword.Image = global::AntivirusUi.Properties.Resources.icons8_password_48;
            this.btnPassword.Location = new System.Drawing.Point(486, 353);
            this.btnPassword.Name = "btnPassword";
            this.btnPassword.Size = new System.Drawing.Size(189, 125);
            this.btnPassword.TabIndex = 8;
            this.btnPassword.Text = "Password Manager";
            this.btnPassword.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnPassword.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnPassword.UseVisualStyleBackColor = false;
            this.btnPassword.Click += new System.EventHandler(this.Password_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            this.btnSettings.BackgroundImage = global::AntivirusUi.Properties.Resources.icons8_settings_50;
            this.btnSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.Color.White;
            this.btnSettings.Location = new System.Drawing.Point(689, 203);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(194, 275);
            this.btnSettings.TabIndex = 7;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // btnFullScan
            // 
            this.btnFullScan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            this.btnFullScan.FlatAppearance.BorderSize = 0;
            this.btnFullScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFullScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFullScan.ForeColor = System.Drawing.Color.White;
            this.btnFullScan.Image = global::AntivirusUi.Properties.Resources.icons8_protect_60;
            this.btnFullScan.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnFullScan.Location = new System.Drawing.Point(52, 353);
            this.btnFullScan.Name = "btnFullScan";
            this.btnFullScan.Size = new System.Drawing.Size(418, 125);
            this.btnFullScan.TabIndex = 6;
            this.btnFullScan.Text = "Full Scan";
            this.btnFullScan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnFullScan.UseVisualStyleBackColor = false;
            this.btnFullScan.Click += new System.EventHandler(this.FullScan_Click);
            // 
            // btnSignature
            // 
            this.btnSignature.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            this.btnSignature.FlatAppearance.BorderSize = 0;
            this.btnSignature.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignature.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignature.ForeColor = System.Drawing.Color.White;
            this.btnSignature.Image = global::AntivirusUi.Properties.Resources.Webp_net_resizeimage;
            this.btnSignature.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSignature.Location = new System.Drawing.Point(371, 203);
            this.btnSignature.Name = "btnSignature";
            this.btnSignature.Size = new System.Drawing.Size(304, 135);
            this.btnSignature.TabIndex = 5;
            this.btnSignature.Text = "Signature Scan";
            this.btnSignature.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSignature.UseVisualStyleBackColor = false;
            this.btnSignature.Click += new System.EventHandler(this.Signature_Click);
            // 
            // btnSandbox
            // 
            this.btnSandbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(64)))), ((int)(((byte)(66)))));
            this.btnSandbox.FlatAppearance.BorderSize = 0;
            this.btnSandbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSandbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSandbox.ForeColor = System.Drawing.Color.White;
            this.btnSandbox.Image = global::AntivirusUi.Properties.Resources.icons8_virtualbox_50;
            this.btnSandbox.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSandbox.Location = new System.Drawing.Point(52, 203);
            this.btnSandbox.Name = "btnSandbox";
            this.btnSandbox.Size = new System.Drawing.Size(304, 135);
            this.btnSandbox.TabIndex = 4;
            this.btnSandbox.Text = "Behavior Scan";
            this.btnSandbox.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnSandbox.UseVisualStyleBackColor = false;
            this.btnSandbox.Click += new System.EventHandler(this.Sandbox_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AntivirusUi.Properties.Resources.icons8_double_tick_100;
            this.pictureBox1.Location = new System.Drawing.Point(53, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(102, 96);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Antivirus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(935, 538);
            this.Controls.Add(this.tvYourName);
            this.Controls.Add(this.btnPassword);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnFullScan);
            this.Controls.Add(this.btnSignature);
            this.Controls.Add(this.btnSandbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Antivirus";
            this.Text = "Antivirus";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSandbox;
        private System.Windows.Forms.Button btnSignature;
        private System.Windows.Forms.Button btnFullScan;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnPassword;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label tvYourName;
    }
}

