﻿using Microsoft.Toolkit.Uwp.Notifications;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace AntivirusUi
{
    internal class RealTimeScanner
    {
        private const string realtime_watcher_path = "..\\..\\..\\core\\realtime_watcher.py";
        private string scanResultLogPath;
        private Process runningScanProcess;
        private Thread check;
        private bool is_running;
        private string[] pythonPaths = {
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\Programs\\Python\\Python37\\python.exe",
            "C:\\Python37\\python.exe"
        };

        public RealTimeScanner()
        {
            is_running = false;
            check = new Thread(() => Check_if_Changed_Params());
            check.SetApartmentState(ApartmentState.STA);
            check.Start();
        }

        private void Check_if_Changed_Params()
        {
            while(true)
            {
                if(!is_running)
                {
                    if (Properties.Settings.Default.isRealTimeEnabled)
                    {
                        Run();
                        is_running = true;
                    }
                }
                if(is_running)
                {
                    if (!Properties.Settings.Default.isRealTimeEnabled)
                    {
                        Stop();
                        is_running = false;
                    }
                }


            }
        }

        public void Run() {
            runningScanProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = File.Exists(pythonPaths[0]) ? pythonPaths[0] : pythonPaths[1],
                    Arguments = string.Format("-u {0}", realtime_watcher_path),
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    Verb = "runas"
                },
                EnableRaisingEvents = true
            };

            runningScanProcess.OutputDataReceived += OnOutputDataRecieved;
            runningScanProcess.ErrorDataReceived += OnErrorDataReceived;

            runningScanProcess.Exited += OnExited;
            runningScanProcess.Start();
            runningScanProcess.BeginOutputReadLine();
            runningScanProcess.BeginErrorReadLine();
            Console.WriteLine("-- Real Time Scanner Running --- ");
        }

        public void Stop() 
        {
            if (!runningScanProcess.HasExited)
                runningScanProcess.Kill();
            Console.WriteLine("-- Real Time Scanner Stopped --- ");
        }

        private void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("LOG: " + e.Data);
        }

        private void OnExited(object sender, EventArgs e) {
            Console.WriteLine("EXITED");
        }

        private void OnOutputDataRecieved(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                CheckScanOutput(e.Data);
            }
        }

        private void CheckScanOutput(string log)
        {
            if (log.StartsWith("log_file: "))
            {
                this.scanResultLogPath = log.Replace("log_file: ", "");
                CheckLog();
            }
        }

        private void End()
        {
            if (!runningScanProcess.HasExited)
                runningScanProcess.Kill();
            check.Abort();
        }

        private void CheckLog() 
        {
            string content = System.IO.File.ReadAllText(scanResultLogPath);

            JObject jsonLog = JObject.Parse(content);

            foreach (JObject res in jsonLog["full_results"])
            {
                this.BuildNotification(res);
            }
            
        }

        private void BuildNotification(JObject single_file_scan_res)
        {
            new ToastContentBuilder()
                .AddText("Scanned Path: " + single_file_scan_res["file_path"])
                .AddText("Threat Name: " + single_file_scan_res["threat_detected"])
                .AddText("Description: " + single_file_scan_res["description"])
                .Show();
        } // Use Microsoft.Toolkit.Uwp.Notifications
    }
}
