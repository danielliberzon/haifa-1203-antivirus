﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntivirusUi
{
    internal class PasswordManager
    {
        private const string password_manager_path = "..\\..\\..\\core\\Password_Saver.py";
        private const string password_transcriptor_path = "..\\..\\..\\core\\Password_Transcriptor.py";
        private Process runningScanProcess;
        private bool correct = true;
        private string[] pythonPaths = {
            Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\Programs\\Python\\Python37\\python.exe",
            "C:\\Python37\\python.exe"
        };
        private List<Account> accounts;
        internal List<Account> Accounts { get => accounts; set => accounts = value; }

        public PasswordManager()
        {
            accounts = new List<Account>();
            this.RunCommand("-all");
        }

        public void RunCommand(string operation, string args="")
        {
            string path = "";
            Console.WriteLine(args);

            path = password_manager_path;
            
            runningScanProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = File.Exists(pythonPaths[0]) ? pythonPaths[0] : pythonPaths[1],
                    Arguments = string.Format("-u {0} {1} {2}", path, operation, args),
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                },
                EnableRaisingEvents = true
            };

            runningScanProcess.OutputDataReceived += OnOutputDataRecieved;
            runningScanProcess.ErrorDataReceived += OnErrorDataReceived;

            runningScanProcess.Start();
            runningScanProcess.BeginOutputReadLine();
            runningScanProcess.BeginErrorReadLine();
            runningScanProcess.WaitForExit();
        }

        public void Stop() { }

        private void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine("LOG: " + e.Data);
        }

        private void OnExited(object sender, EventArgs e)
        {
            Console.WriteLine("EXITED");
        }

        private void OnOutputDataRecieved(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                CheckScanOutput(e.Data);
            }
        }

        private void CheckScanOutput(string log)
        {
            if (log.StartsWith("-f"))
            {
                Console.WriteLine(log);
                string[] accDetails = log.Substring(3, log.Length - 4).Split(',');
                Account newAccount = new Account(accDetails[1], accDetails[2], accDetails[3], accDetails[4]);
                this.accounts.Add(newAccount);
            }
            else
            {
                if (log.StartsWith("-i"))
                {
                    correct = false;
                }
                Console.WriteLine(log);
            }
        }

        public bool GetPasswordState()
        {
            return this.correct;
        }
    }
}
