﻿
namespace AntivirusUi
{
    partial class SettingsScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvProtectedParagraph = new System.Windows.Forms.Label();
            this.tvSettingsTitle = new System.Windows.Forms.Label();
            this.tvScheduledScanSubtitle = new System.Windows.Forms.Label();
            this.tvChooseScanFreqSubtitle = new System.Windows.Forms.Label();
            this.ivProfilePic = new System.Windows.Forms.PictureBox();
            this.tbScanFreq = new System.Windows.Forms.TrackBar();
            this.tvDaily = new System.Windows.Forms.Label();
            this.tvWeekly = new System.Windows.Forms.Label();
            this.tvFolderScanSubtitle = new System.Windows.Forms.Label();
            this.btnChooseFolder = new System.Windows.Forms.Button();
            this.tvRealTimeSubtitle = new System.Windows.Forms.Label();
            this.cbEnableRealTime = new System.Windows.Forms.CheckBox();
            this.btnApplySettings = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.etUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tvNone = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ivProfilePic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbScanFreq)).BeginInit();
            this.SuspendLayout();
            // 
            // tvProtectedParagraph
            // 
            this.tvProtectedParagraph.AutoSize = true;
            this.tvProtectedParagraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvProtectedParagraph.ForeColor = System.Drawing.Color.White;
            this.tvProtectedParagraph.Location = new System.Drawing.Point(187, 84);
            this.tvProtectedParagraph.Name = "tvProtectedParagraph";
            this.tvProtectedParagraph.Size = new System.Drawing.Size(63, 13);
            this.tvProtectedParagraph.TabIndex = 2;
            this.tvProtectedParagraph.Text = "Is protected";
            // 
            // tvSettingsTitle
            // 
            this.tvSettingsTitle.AutoSize = true;
            this.tvSettingsTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.75F, System.Drawing.FontStyle.Bold);
            this.tvSettingsTitle.ForeColor = System.Drawing.Color.White;
            this.tvSettingsTitle.Location = new System.Drawing.Point(167, 152);
            this.tvSettingsTitle.Name = "tvSettingsTitle";
            this.tvSettingsTitle.Size = new System.Drawing.Size(90, 24);
            this.tvSettingsTitle.TabIndex = 3;
            this.tvSettingsTitle.Text = "Settings:";
            this.tvSettingsTitle.Click += new System.EventHandler(this.tvSettingsTitle_Click);
            // 
            // tvScheduledScanSubtitle
            // 
            this.tvScheduledScanSubtitle.AutoSize = true;
            this.tvScheduledScanSubtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.tvScheduledScanSubtitle.ForeColor = System.Drawing.Color.White;
            this.tvScheduledScanSubtitle.Location = new System.Drawing.Point(12, 197);
            this.tvScheduledScanSubtitle.Name = "tvScheduledScanSubtitle";
            this.tvScheduledScanSubtitle.Size = new System.Drawing.Size(128, 16);
            this.tvScheduledScanSubtitle.TabIndex = 4;
            this.tvScheduledScanSubtitle.Text = "Scheduled Scans";
            // 
            // tvChooseScanFreqSubtitle
            // 
            this.tvChooseScanFreqSubtitle.AutoSize = true;
            this.tvChooseScanFreqSubtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.749999F, System.Drawing.FontStyle.Bold);
            this.tvChooseScanFreqSubtitle.ForeColor = System.Drawing.Color.White;
            this.tvChooseScanFreqSubtitle.Location = new System.Drawing.Point(12, 225);
            this.tvChooseScanFreqSubtitle.Name = "tvChooseScanFreqSubtitle";
            this.tvChooseScanFreqSubtitle.Size = new System.Drawing.Size(149, 13);
            this.tvChooseScanFreqSubtitle.TabIndex = 5;
            this.tvChooseScanFreqSubtitle.Text = "Choose Scan Frequency:";
            this.tvChooseScanFreqSubtitle.Click += new System.EventHandler(this.tvChooseScanFreqSubtitle_Click);
            // 
            // ivProfilePic
            // 
            this.ivProfilePic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ivProfilePic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ivProfilePic.Cursor = System.Windows.Forms.Cursors.Default;
            this.ivProfilePic.Image = global::AntivirusUi.Properties.Resources.dummy_profile_picture;
            this.ivProfilePic.Location = new System.Drawing.Point(70, 32);
            this.ivProfilePic.Name = "ivProfilePic";
            this.ivProfilePic.Size = new System.Drawing.Size(104, 95);
            this.ivProfilePic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ivProfilePic.TabIndex = 0;
            this.ivProfilePic.TabStop = false;
            // 
            // tbScanFreq
            // 
            this.tbScanFreq.LargeChange = 1;
            this.tbScanFreq.Location = new System.Drawing.Point(36, 243);
            this.tbScanFreq.Maximum = 3;
            this.tbScanFreq.Name = "tbScanFreq";
            this.tbScanFreq.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbScanFreq.Size = new System.Drawing.Size(45, 106);
            this.tbScanFreq.TabIndex = 6;
            this.tbScanFreq.Value = 3;
            this.tbScanFreq.Scroll += new System.EventHandler(this.tbScanFreq_Scroll);
            // 
            // tvDaily
            // 
            this.tvDaily.AutoSize = true;
            this.tvDaily.ForeColor = System.Drawing.Color.White;
            this.tvDaily.Location = new System.Drawing.Point(64, 276);
            this.tvDaily.Name = "tvDaily";
            this.tvDaily.Size = new System.Drawing.Size(30, 13);
            this.tvDaily.TabIndex = 7;
            this.tvDaily.Text = "Daily";
            this.tvDaily.Click += new System.EventHandler(this.tvDaily_Click);
            // 
            // tvWeekly
            // 
            this.tvWeekly.AutoSize = true;
            this.tvWeekly.ForeColor = System.Drawing.Color.White;
            this.tvWeekly.Location = new System.Drawing.Point(63, 303);
            this.tvWeekly.Name = "tvWeekly";
            this.tvWeekly.Size = new System.Drawing.Size(43, 13);
            this.tvWeekly.TabIndex = 8;
            this.tvWeekly.Text = "Weekly";
            this.tvWeekly.Click += new System.EventHandler(this.tvWeekly_Click);
            // 
            // tvFolderScanSubtitle
            // 
            this.tvFolderScanSubtitle.AutoSize = true;
            this.tvFolderScanSubtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.749999F, System.Drawing.FontStyle.Bold);
            this.tvFolderScanSubtitle.ForeColor = System.Drawing.Color.White;
            this.tvFolderScanSubtitle.Location = new System.Drawing.Point(12, 368);
            this.tvFolderScanSubtitle.Name = "tvFolderScanSubtitle";
            this.tvFolderScanSubtitle.Size = new System.Drawing.Size(140, 13);
            this.tvFolderScanSubtitle.TabIndex = 10;
            this.tvFolderScanSubtitle.Text = "Choose Folder to Scan:";
            // 
            // btnChooseFolder
            // 
            this.btnChooseFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChooseFolder.ForeColor = System.Drawing.Color.White;
            this.btnChooseFolder.Location = new System.Drawing.Point(36, 395);
            this.btnChooseFolder.Name = "btnChooseFolder";
            this.btnChooseFolder.Size = new System.Drawing.Size(225, 23);
            this.btnChooseFolder.TabIndex = 11;
            this.btnChooseFolder.Text = "Choose Folder";
            this.btnChooseFolder.UseVisualStyleBackColor = true;
            this.btnChooseFolder.Click += new System.EventHandler(this.btnChooseFolder_Click);
            // 
            // tvRealTimeSubtitle
            // 
            this.tvRealTimeSubtitle.AutoSize = true;
            this.tvRealTimeSubtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.tvRealTimeSubtitle.ForeColor = System.Drawing.Color.White;
            this.tvRealTimeSubtitle.Location = new System.Drawing.Point(12, 452);
            this.tvRealTimeSubtitle.Name = "tvRealTimeSubtitle";
            this.tvRealTimeSubtitle.Size = new System.Drawing.Size(118, 16);
            this.tvRealTimeSubtitle.TabIndex = 12;
            this.tvRealTimeSubtitle.Text = "Real Time Scan";
            // 
            // cbEnableRealTime
            // 
            this.cbEnableRealTime.AutoSize = true;
            this.cbEnableRealTime.Checked = true;
            this.cbEnableRealTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEnableRealTime.ForeColor = System.Drawing.Color.White;
            this.cbEnableRealTime.Location = new System.Drawing.Point(36, 480);
            this.cbEnableRealTime.Name = "cbEnableRealTime";
            this.cbEnableRealTime.Size = new System.Drawing.Size(138, 17);
            this.cbEnableRealTime.TabIndex = 13;
            this.cbEnableRealTime.Text = "Enable Real-Time Scan";
            this.cbEnableRealTime.UseVisualStyleBackColor = true;
            this.cbEnableRealTime.CheckedChanged += new System.EventHandler(this.cbEnableRealTime_CheckedChanged);
            // 
            // btnApplySettings
            // 
            this.btnApplySettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApplySettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnApplySettings.ForeColor = System.Drawing.Color.DarkGray;
            this.btnApplySettings.Location = new System.Drawing.Point(333, 632);
            this.btnApplySettings.Name = "btnApplySettings";
            this.btnApplySettings.Size = new System.Drawing.Size(100, 27);
            this.btnApplySettings.TabIndex = 16;
            this.btnApplySettings.Text = "Apply";
            this.btnApplySettings.UseVisualStyleBackColor = true;
            this.btnApplySettings.Click += new System.EventHandler(this.btnApplySettings_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(227, 632);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 27);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // etUsername
            // 
            this.etUsername.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(40)))));
            this.etUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.etUsername.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.etUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.75F, System.Drawing.FontStyle.Bold);
            this.etUsername.ForeColor = System.Drawing.Color.White;
            this.etUsername.Location = new System.Drawing.Point(190, 54);
            this.etUsername.MaxLength = 15;
            this.etUsername.Multiline = true;
            this.etUsername.Name = "etUsername";
            this.etUsername.Size = new System.Drawing.Size(233, 43);
            this.etUsername.TabIndex = 18;
            this.etUsername.Text = "Your Name";
            this.etUsername.TextChanged += new System.EventHandler(this.etUsername_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(63, 329);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Monthly";
            // 
            // tvNone
            // 
            this.tvNone.AutoSize = true;
            this.tvNone.ForeColor = System.Drawing.Color.White;
            this.tvNone.Location = new System.Drawing.Point(64, 250);
            this.tvNone.Name = "tvNone";
            this.tvNone.Size = new System.Drawing.Size(33, 13);
            this.tvNone.TabIndex = 20;
            this.tvNone.Text = "None";
            // 
            // SettingsScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(445, 671);
            this.Controls.Add(this.tvNone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tvProtectedParagraph);
            this.Controls.Add(this.etUsername);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApplySettings);
            this.Controls.Add(this.cbEnableRealTime);
            this.Controls.Add(this.tvRealTimeSubtitle);
            this.Controls.Add(this.btnChooseFolder);
            this.Controls.Add(this.tvFolderScanSubtitle);
            this.Controls.Add(this.tvWeekly);
            this.Controls.Add(this.tvDaily);
            this.Controls.Add(this.tbScanFreq);
            this.Controls.Add(this.tvChooseScanFreqSubtitle);
            this.Controls.Add(this.tvScheduledScanSubtitle);
            this.Controls.Add(this.tvSettingsTitle);
            this.Controls.Add(this.ivProfilePic);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SettingsScreen";
            this.Text = "SettingsScreen";
            this.Load += new System.EventHandler(this.SettingsScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ivProfilePic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbScanFreq)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ivProfilePic;
        private System.Windows.Forms.Label tvProtectedParagraph;
        private System.Windows.Forms.Label tvSettingsTitle;
        private System.Windows.Forms.Label tvScheduledScanSubtitle;
        private System.Windows.Forms.Label tvChooseScanFreqSubtitle;
        private System.Windows.Forms.TrackBar tbScanFreq;
        private System.Windows.Forms.Label tvDaily;
        private System.Windows.Forms.Label tvWeekly;
        private System.Windows.Forms.Label tvFolderScanSubtitle;
        private System.Windows.Forms.Button btnChooseFolder;
        private System.Windows.Forms.Label tvRealTimeSubtitle;
        private System.Windows.Forms.CheckBox cbEnableRealTime;
        private System.Windows.Forms.Button btnApplySettings;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox etUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label tvNone;
    }
}