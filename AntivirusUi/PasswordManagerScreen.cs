﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;
using Windows.UI.Xaml.Media.Animation;

namespace AntivirusUi
{
    public partial class PasswordManagerScreen : Form
    {
        private PasswordManager passwordManager;
        private List<ListItem> allAccounts;
        private Account chosen;

        const string DEFAULT_ACCOUNT_LOGO_URL = "https://www.babson.edu/media/babson/assets/homepage-images/Diff-Globe-370-04.png";
        public PasswordManagerScreen()
        {
            InitializeComponent();
            if(!File.Exists("password_data.db"))
            {
                this.label5.Text = "Please Enter A New Master Password:";
            }
            this.passwordManager = new PasswordManager();
            this.allAccounts = new List<ListItem>();
            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
        }

        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            passwordManager.RunCommand("-ed", string.Format("{0} {1} {2} {3}", tbPassword.Text, 1, 0, 1));
        }

        private void btCheckPass_Click(object sender, EventArgs e)
        {
            if(this.tbPassword.Text != "")
            {
                if (this.label5.Text == "Please Enter A New Master Password:")
                {
                    passwordManager.RunCommand("-a", string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\"", "PasswordTester", "test", "test", tbPassword.Text));
                    if (passwordManager.GetPasswordState())
                    {
                        this.pAccountDetails.Visible = true;
                        this.flAccountsList.Visible = true;
                        this.panel1.Visible = false;
                    }
                }
                else
                {
                    passwordManager.RunCommand("-ed", string.Format("{0} {1} {2} {3}", tbPassword.Text, 0, 0, 0));
                    if (passwordManager.GetPasswordState())
                    {
                        this.panel1.Visible = false;
                        this.pAccountDetails.Visible = true;
                        this.flAccountsList.Visible = true;
                        PasswordManagerScreen_Load();
                    }
                }

            }

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void PasswordManagerScreen_Load()
        {
            populateAccounts();

            ScrollBar vScrollBar1 = new VScrollBar();
            vScrollBar1.Dock = DockStyle.Left;
            vScrollBar1.Scroll += VScrollBar1_Scroll;
            //flAccountsList.Controls.Add(vScrollBar1);
            
        }

        private void VScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            flAccountsList.VerticalScroll.Value = ((ScrollBar)sender).Value;
        }

        private void populateAccounts()
        {
            for (int i = 0; i < this.passwordManager.Accounts.Count; i++)
            {
                Console.WriteLine("-- Adding Account --");
                allAccounts.Add(new ListItem());
                Console.WriteLine(passwordManager.Accounts[i].Title);
                allAccounts[i].AccountTitle = this.passwordManager.Accounts[i].Title;
                allAccounts[i].Url = this.passwordManager.Accounts[i].Url;
                allAccounts[i].SiteLogo = this.passwordManager.Accounts[i].SiteLogoPath;
                allAccounts[i].Cursor = Cursors.Hand;
                allAccounts[i].pBg.Tag = i;
                allAccounts[i].pBg.Click += ShowAccountPanel;

                flAccountsList.Controls.Add(allAccounts[i]);
            }
        }

        private void ShowAccountPanel(object sender, EventArgs e)
        {
            Panel selected = (Panel)sender;
            this.chosen = this.passwordManager.Accounts[(int)selected.Tag];

            tbAccTitle.Text = chosen.Title;
            tbAccLogin.Text = chosen.Login;
            tbAccPassword.Text = chosen.Password;
            tbAccUrl.Text = chosen.Url;
            ivAccLogo.Load(chosen.SiteLargeLogoPath);

            Transitions.Transition t = new Transitions.Transition(new TransitionType_Linear(100));

            ShowAccountDetails(chosen);

            t.add(pAccountDetails, "Left", this.Size.Width - pAccountDetails.Width);
            t.run();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        { 
                if (btnEdit.Text == "Create")
                {
                    // Update Database
                    passwordManager.RunCommand("-a", string.Format("\"{0}\" {1} \"{2}\" {3}", tbAccTitle.Text, tbAccUrl.Text, tbAccLogin.Text, tbAccPassword.Text));
                    chosen = new Account(tbAccTitle.Text, tbAccUrl.Text, tbAccLogin.Text, tbAccPassword.Text);
                    this.passwordManager.Accounts.Add(chosen);

                    ListItem newItem = new ListItem();
                    newItem.AccountTitle = chosen.Title;
                    newItem.Url = chosen.Url;
                    newItem.SiteLogo = chosen.SiteLogoPath;
                    newItem.Cursor = Cursors.Hand;
                    newItem.pBg.Tag = allAccounts.Count;
                    newItem.pBg.Click += ShowAccountPanel;
                    ivAccLogo.Load(chosen.SiteLargeLogoPath);

                    allAccounts.Add(newItem);
                    flAccountsList.Controls.Add(newItem);
                    btnEdit.Text = "Edit";
                }
                else if (btnEdit.Text == "Apply")
                {
                    ListItem item = allAccounts.Find(x => x.AccountTitle == chosen.Title);

                    // Update Database
                    passwordManager.RunCommand("-u", string.Format("\"{0}\" \"{1}\" \"{2}\" {3}", tbAccTitle.Text, tbAccUrl.Text, tbAccLogin.Text, tbAccPassword.Text));

                    // Update local list of accounts
                    chosen.Login = tbAccLogin.Text;
                    chosen.ChangeSiteUrl(tbAccUrl.Text);
                    chosen.Password = tbAccPassword.Text;
                    chosen.Title = tbAccTitle.Text;

                    // Update new changes in the user interface
                    item.tvAccountTitle.Text = chosen.Title;
                    item.tvUrl.Text = chosen.Url;
                    item.SiteLogo = chosen.SiteLogoPath;
                    ivAccLogo.Load(chosen.SiteLargeLogoPath);
                    btnEdit.Text = "Edit";

                    tbAccTitle.ReadOnly = true;
                    tbAccLogin.ReadOnly = true;
                    tbAccPassword.ReadOnly = true;
                    tbAccUrl.ReadOnly = true;
                }
                else
                {
                    tbAccTitle.ReadOnly = false;
                    tbAccLogin.ReadOnly = false;
                    tbAccPassword.ReadOnly = false;
                    tbAccUrl.ReadOnly = false;
                    btnEdit.Text = "Apply";
                }
        }

        private void ShowAccountDetails(Account account)
        {
            tbAccTitle.Text = account.Title;
            tbAccLogin.Text = account.Login;
            tbAccPassword.Text = account.Password;
            tbAccUrl.Text = account.Url;
            ivAccLogo.Load(account.SiteLargeLogoPath);
            btnEdit.Text = "Edit";
        }

        private bool validAccountDetails()
        {
            return tbAccTitle.Text != String.Empty
                && tbAccLogin.Text != String.Empty
                && tbAccPassword.Text != String.Empty
                && tbAccUrl.Text != String.Empty;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (allAccounts.Count > 0)
            {
                ListItem item = allAccounts.Find(x => x.AccountTitle == chosen.Title);

                passwordManager.RunCommand("-d", string.Format("\"{0}\"", item.AccountTitle));
                passwordManager.Accounts.Remove(chosen);
                allAccounts.Remove(item);
                flAccountsList.Controls.Remove(item);

                for (int i = 0; i < allAccounts.Count; i++)
                {
                    allAccounts[i].pBg.Tag = i;
                }

                if (passwordManager.Accounts.Count > 0)
                {
                    chosen = passwordManager.Accounts.Last();
                    ShowAccountDetails(passwordManager.Accounts.Last());
                }
            }
            if (allAccounts.Count == 0)
            {
                Transitions.Transition t = new Transitions.Transition(new TransitionType_Linear(100));
                t.add(pAccountDetails, "Left", this.Size.Width + pAccountDetails.Width);
                t.run();
            }
        }

        private void tbAccUrl_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkCanApplyDetails(object sender, EventArgs e)
        {
            btnEdit.Enabled = validAccountDetails();
        }

        private void flAccountsList_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void btnAddAccount_Click(object sender, EventArgs e)
        {
            var client = new System.Net.WebClient();

            tbAccTitle.ReadOnly = false;
            tbAccLogin.ReadOnly = false;
            tbAccPassword.ReadOnly = false;
            tbAccUrl.ReadOnly = false;

            tbAccTitle.Text = "New Title";
            tbAccLogin.Text = "...";
            tbAccPassword.Text = "...";
            tbAccUrl.Text = "...";

            if (!Directory.Exists("siteImages"))
            {
                Directory.CreateDirectory("siteImages");

                if (!File.Exists("siteImages\\default_icon.png"))
                {
                    client.DownloadFile(DEFAULT_ACCOUNT_LOGO_URL, "siteImages\\default_icon.png");
                }
            }

            ivAccLogo.Load("siteImages\\default_icon.png");
            btnEdit.Text = "Create";

            Transitions.Transition t = new Transitions.Transition(new TransitionType_Linear(100));
            t.add(pAccountDetails, "Left", this.Size.Width - pAccountDetails.Width);
            t.run();

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void pAccountDetails_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
