﻿namespace AntivirusUi
{
    partial class SignatureScanScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvFilePath = new System.Windows.Forms.Label();
            this.tvFinishedItems = new System.Windows.Forms.Label();
            this.tvElapsedTime = new System.Windows.Forms.Label();
            this.tvFoundMalicious = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.tvHeadLine = new System.Windows.Forms.Label();
            this.btnChooseFile = new System.Windows.Forms.Button();
            this.tvCurrentFoundMalicious = new System.Windows.Forms.Label();
            this.tvCurrentElapsedTime = new System.Windows.Forms.Label();
            this.tvCurrentFinishedItems = new System.Windows.Forms.Label();
            this.tvCurrentFile = new System.Windows.Forms.Label();
            this.btnChooseFolder = new System.Windows.Forms.Button();
            this.btnShowLogs = new System.Windows.Forms.Button();
            this.tvProcess = new System.Windows.Forms.Label();
            this.tvOutput = new System.Windows.Forms.Label();
            this.IvScanning = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.IvScanning)).BeginInit();
            this.SuspendLayout();
            // 
            // tvFilePath
            // 
            this.tvFilePath.AutoSize = true;
            this.tvFilePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvFilePath.ForeColor = System.Drawing.Color.White;
            this.tvFilePath.Location = new System.Drawing.Point(334, 118);
            this.tvFilePath.Name = "tvFilePath";
            this.tvFilePath.Size = new System.Drawing.Size(32, 16);
            this.tvFilePath.TabIndex = 0;
            this.tvFilePath.Text = "File:";
            // 
            // tvFinishedItems
            // 
            this.tvFinishedItems.AutoSize = true;
            this.tvFinishedItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvFinishedItems.ForeColor = System.Drawing.Color.White;
            this.tvFinishedItems.Location = new System.Drawing.Point(334, 150);
            this.tvFinishedItems.Name = "tvFinishedItems";
            this.tvFinishedItems.Size = new System.Drawing.Size(96, 16);
            this.tvFinishedItems.TabIndex = 1;
            this.tvFinishedItems.Text = "Finished Items:";
            // 
            // tvElapsedTime
            // 
            this.tvElapsedTime.AutoSize = true;
            this.tvElapsedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvElapsedTime.ForeColor = System.Drawing.Color.White;
            this.tvElapsedTime.Location = new System.Drawing.Point(334, 181);
            this.tvElapsedTime.Name = "tvElapsedTime";
            this.tvElapsedTime.Size = new System.Drawing.Size(95, 16);
            this.tvElapsedTime.TabIndex = 1;
            this.tvElapsedTime.Text = "Elapsed Time:";
            // 
            // tvFoundMalicious
            // 
            this.tvFoundMalicious.AutoSize = true;
            this.tvFoundMalicious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvFoundMalicious.ForeColor = System.Drawing.Color.White;
            this.tvFoundMalicious.Location = new System.Drawing.Point(334, 215);
            this.tvFoundMalicious.Name = "tvFoundMalicious";
            this.tvFoundMalicious.Size = new System.Drawing.Size(108, 16);
            this.tvFoundMalicious.TabIndex = 1;
            this.tvFoundMalicious.Text = "Found Malicious:";
            // 
            // btnStart
            // 
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btnStart.Location = new System.Drawing.Point(153, 369);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(416, 43);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.StartScan);
            // 
            // tvHeadLine
            // 
            this.tvHeadLine.AutoSize = true;
            this.tvHeadLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvHeadLine.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.tvHeadLine.Location = new System.Drawing.Point(329, 51);
            this.tvHeadLine.Name = "tvHeadLine";
            this.tvHeadLine.Size = new System.Drawing.Size(292, 39);
            this.tvHeadLine.TabIndex = 4;
            this.tvHeadLine.Text = "Waiting to Start..";
            // 
            // btnChooseFile
            // 
            this.btnChooseFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChooseFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseFile.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btnChooseFile.Location = new System.Drawing.Point(153, 336);
            this.btnChooseFile.Name = "btnChooseFile";
            this.btnChooseFile.Size = new System.Drawing.Size(202, 27);
            this.btnChooseFile.TabIndex = 5;
            this.btnChooseFile.Text = "Choose File to Scan..";
            this.btnChooseFile.UseVisualStyleBackColor = true;
            this.btnChooseFile.Click += new System.EventHandler(this.ChooseElementToScan);
            // 
            // tvCurrentFoundMalicious
            // 
            this.tvCurrentFoundMalicious.AutoSize = true;
            this.tvCurrentFoundMalicious.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvCurrentFoundMalicious.ForeColor = System.Drawing.Color.White;
            this.tvCurrentFoundMalicious.Location = new System.Drawing.Point(471, 215);
            this.tvCurrentFoundMalicious.Name = "tvCurrentFoundMalicious";
            this.tvCurrentFoundMalicious.Size = new System.Drawing.Size(0, 16);
            this.tvCurrentFoundMalicious.TabIndex = 7;
            // 
            // tvCurrentElapsedTime
            // 
            this.tvCurrentElapsedTime.AutoSize = true;
            this.tvCurrentElapsedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvCurrentElapsedTime.ForeColor = System.Drawing.Color.White;
            this.tvCurrentElapsedTime.Location = new System.Drawing.Point(471, 181);
            this.tvCurrentElapsedTime.Name = "tvCurrentElapsedTime";
            this.tvCurrentElapsedTime.Size = new System.Drawing.Size(0, 16);
            this.tvCurrentElapsedTime.TabIndex = 8;
            // 
            // tvCurrentFinishedItems
            // 
            this.tvCurrentFinishedItems.AutoSize = true;
            this.tvCurrentFinishedItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvCurrentFinishedItems.ForeColor = System.Drawing.Color.White;
            this.tvCurrentFinishedItems.Location = new System.Drawing.Point(471, 150);
            this.tvCurrentFinishedItems.Name = "tvCurrentFinishedItems";
            this.tvCurrentFinishedItems.Size = new System.Drawing.Size(0, 16);
            this.tvCurrentFinishedItems.TabIndex = 9;
            // 
            // tvCurrentFile
            // 
            this.tvCurrentFile.AutoSize = true;
            this.tvCurrentFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvCurrentFile.ForeColor = System.Drawing.Color.White;
            this.tvCurrentFile.Location = new System.Drawing.Point(471, 118);
            this.tvCurrentFile.Name = "tvCurrentFile";
            this.tvCurrentFile.Size = new System.Drawing.Size(0, 16);
            this.tvCurrentFile.TabIndex = 6;
            // 
            // btnChooseFolder
            // 
            this.btnChooseFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChooseFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChooseFolder.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btnChooseFolder.Location = new System.Drawing.Point(367, 336);
            this.btnChooseFolder.Name = "btnChooseFolder";
            this.btnChooseFolder.Size = new System.Drawing.Size(202, 27);
            this.btnChooseFolder.TabIndex = 10;
            this.btnChooseFolder.Text = "Choose Folder to Scan..";
            this.btnChooseFolder.UseVisualStyleBackColor = true;
            this.btnChooseFolder.Click += new System.EventHandler(this.ChooseElementToScan);
            // 
            // btnShowLogs
            // 
            this.btnShowLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowLogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowLogs.ForeColor = System.Drawing.Color.Crimson;
            this.btnShowLogs.Location = new System.Drawing.Point(588, 383);
            this.btnShowLogs.Name = "btnShowLogs";
            this.btnShowLogs.Size = new System.Drawing.Size(170, 29);
            this.btnShowLogs.TabIndex = 12;
            this.btnShowLogs.Text = "View Logs";
            this.btnShowLogs.UseVisualStyleBackColor = true;
            this.btnShowLogs.Click += new System.EventHandler(this.ShowLogs);
            // 
            // tvProcess
            // 
            this.tvProcess.BackColor = System.Drawing.Color.Transparent;
            this.tvProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvProcess.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvProcess.ForeColor = System.Drawing.Color.White;
            this.tvProcess.Location = new System.Drawing.Point(588, 245);
            this.tvProcess.Name = "tvProcess";
            this.tvProcess.Size = new System.Drawing.Size(283, 118);
            this.tvProcess.TabIndex = 13;
            // 
            // tvOutput
            // 
            this.tvOutput.AutoSize = true;
            this.tvOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvOutput.ForeColor = System.Drawing.Color.White;
            this.tvOutput.Location = new System.Drawing.Point(585, 230);
            this.tvOutput.Name = "tvOutput";
            this.tvOutput.Size = new System.Drawing.Size(83, 13);
            this.tvOutput.TabIndex = 14;
            this.tvOutput.Text = "Console Output:";
            // 
            // IvScanning
            // 
            this.IvScanning.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.IvScanning.Cursor = System.Windows.Forms.Cursors.Default;
            this.IvScanning.Image = global::AntivirusUi.Properties.Resources.loading;
            this.IvScanning.Location = new System.Drawing.Point(53, 34);
            this.IvScanning.Name = "IvScanning";
            this.IvScanning.Size = new System.Drawing.Size(245, 240);
            this.IvScanning.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.IvScanning.TabIndex = 11;
            this.IvScanning.TabStop = false;
            // 
            // SignatureScanScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(936, 458);
            this.Controls.Add(this.tvOutput);
            this.Controls.Add(this.tvProcess);
            this.Controls.Add(this.btnShowLogs);
            this.Controls.Add(this.IvScanning);
            this.Controls.Add(this.btnChooseFolder);
            this.Controls.Add(this.tvCurrentFoundMalicious);
            this.Controls.Add(this.tvCurrentElapsedTime);
            this.Controls.Add(this.tvCurrentFinishedItems);
            this.Controls.Add(this.tvCurrentFile);
            this.Controls.Add(this.btnChooseFile);
            this.Controls.Add(this.tvHeadLine);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tvFoundMalicious);
            this.Controls.Add(this.tvElapsedTime);
            this.Controls.Add(this.tvFinishedItems);
            this.Controls.Add(this.tvFilePath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SignatureScanScreen";
            this.Text = "SignatureScanScreen";
            ((System.ComponentModel.ISupportInitialize)(this.IvScanning)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tvFilePath;
        private System.Windows.Forms.Label tvFinishedItems;
        private System.Windows.Forms.Label tvElapsedTime;
        private System.Windows.Forms.Label tvFoundMalicious;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label tvHeadLine;
        private System.Windows.Forms.Button btnChooseFile;
        private System.Windows.Forms.Label tvCurrentFoundMalicious;
        private System.Windows.Forms.Label tvCurrentElapsedTime;
        private System.Windows.Forms.Label tvCurrentFinishedItems;
        private System.Windows.Forms.Label tvCurrentFile;
        private System.Windows.Forms.Button btnChooseFolder;
        private System.Windows.Forms.PictureBox IvScanning;
        private System.Windows.Forms.Button btnShowLogs;
        private System.Windows.Forms.Label tvProcess;
        private System.Windows.Forms.Label tvOutput;
    }
}