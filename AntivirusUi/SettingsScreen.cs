﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntivirusUi
{
    public partial class SettingsScreen : Form
    {
        public SettingsScreen()
        {
            InitializeComponent();
            etUsername.KeyDown += EtUsername_KeyDown;
            btnApplySettings.EnabledChanged += BtnApplySettings_EnabledChanged;
        }

        private void EtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
            }         
        }

        private void BtnApplySettings_EnabledChanged(object sender, EventArgs e)
        {
            btnApplySettings.ForeColor = btnApplySettings.Enabled ? Color.CornflowerBlue : Color.Black;
        }

        private void SettingsScreen_Load(object sender, EventArgs e)
        {
            etUsername.Text = Properties.Settings.Default.Username;
            btnChooseFolder.Text = Properties.Settings.Default.folderPathToScan;
            cbEnableRealTime.Checked = Properties.Settings.Default.isRealTimeEnabled;
            tbScanFreq.Value = Properties.Settings.Default.scheduledFreq;
            btnApplySettings.Enabled = false;
        }

        private void btnApplySettings_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Username = etUsername.Text;
            Properties.Settings.Default.folderPathToScan = btnChooseFolder.Text;
            Properties.Settings.Default.isRealTimeEnabled = cbEnableRealTime.Checked;
            Properties.Settings.Default.scheduledFreq = tbScanFreq.Value;
            Properties.Settings.Default.Username = etUsername.Text;

            btnApplySettings.Enabled = false;
            Properties.Settings.Default.Save();
        }

        private void btnChooseFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                btnChooseFolder.Text = openFileDialog.SelectedPath;
                btnApplySettings.Enabled = true;
            }
        }

        private void tbScanFreq_Scroll(object sender, EventArgs e)
        {
            btnApplySettings.Enabled = true;
        }

        private void cbEnableRealTime_CheckedChanged(object sender, EventArgs e)
        {
            btnApplySettings.Enabled = true;
        }

        private void etUsername_TextChanged(object sender, EventArgs e)
        {
            btnApplySettings.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tvChooseScanFreqSubtitle_Click(object sender, EventArgs e)
        {

        }

        private void tvDaily_Click(object sender, EventArgs e)
        {

        }

        private void tvSettingsTitle_Click(object sender, EventArgs e)
        {

        }

        private void tvWeekly_Click(object sender, EventArgs e)
        {

        }
    }
}
