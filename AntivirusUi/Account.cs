﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace AntivirusUi
{
    internal class Account
    {
        private string title;
        private string login;
        private string password;
        private string url;
        private string siteLogoPath;
        private string siteLargeLogoPath;
        const string DEFAULT_ACCOUNT_LOGO_URL = "https://www.babson.edu/media/babson/assets/homepage-images/Diff-Globe-370-04.png";
        public Account(string title, string url, string login , string password)
        {
            this.title = title.Trim();
            this.login = login.Trim();
            this.password = password.Trim();
            this.url = url.Trim();
            this.siteLogoPath = FetchSiteIcon(url.Trim(), 40);
            this.siteLargeLogoPath = FetchSiteIcon(url.Trim(), 85);
        }

        public string FetchSiteIcon(string url, int size)
        {
            var client = new System.Net.WebClient();

            if (!Directory.Exists("siteImages"))
            {
                Directory.CreateDirectory("siteImages");
            }
            try
            {
                if (!File.Exists(string.Format("siteImages\\{0}{1}.ico", url, size)))
                {
                    client.DownloadFile(string.Format("https://logo.clearbit.com/{0}?size={1}", url, size), string.Format("siteImages\\{0}{1}.ico", url, size));
                }
                return string.Format("siteImages\\{0}{1}.ico", url, size);
            }
            catch (System.Net.WebException)
            {
                if (!File.Exists("siteImages\\default_icon.png"))
                {
                    client.DownloadFile(DEFAULT_ACCOUNT_LOGO_URL, "siteImages\\default_icon.png");
                }
            }
            return "siteImages\\default_icon.png";
        }

        public void ChangeSiteUrl(string newUrl)
        {
            this.url = newUrl;
            this.siteLogoPath = FetchSiteIcon(newUrl.Trim(), 40);
            this.siteLargeLogoPath = FetchSiteIcon(newUrl.Trim(), 85);
        }

        public string Title { get => title; set => title = value; }
        public string Login { get => login; set => login = value; }
        public string Password { get => password; set => password = value; }
        public string Url { get => url; }
        public string SiteLogoPath { get => siteLogoPath; }
        public string SiteLargeLogoPath { get => siteLargeLogoPath; }
    }
}
