﻿namespace AntivirusUi
{
    partial class ListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvAccountTitle = new System.Windows.Forms.Label();
            this.tvUrl = new System.Windows.Forms.Label();
            this.pBg = new System.Windows.Forms.Panel();
            this.ivLogo = new System.Windows.Forms.PictureBox();
            this.pBg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ivLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tvAccountTitle
            // 
            this.tvAccountTitle.AutoSize = true;
            this.tvAccountTitle.Font = new System.Drawing.Font("Roboto", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvAccountTitle.ForeColor = System.Drawing.Color.White;
            this.tvAccountTitle.Location = new System.Drawing.Point(61, 16);
            this.tvAccountTitle.Name = "tvAccountTitle";
            this.tvAccountTitle.Size = new System.Drawing.Size(55, 25);
            this.tvAccountTitle.TabIndex = 1;
            this.tvAccountTitle.Text = "Title";
            // 
            // tvUrl
            // 
            this.tvUrl.AutoSize = true;
            this.tvUrl.Font = new System.Drawing.Font("Roboto", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvUrl.ForeColor = System.Drawing.Color.White;
            this.tvUrl.Location = new System.Drawing.Point(61, 43);
            this.tvUrl.Name = "tvUrl";
            this.tvUrl.Size = new System.Drawing.Size(65, 13);
            this.tvUrl.TabIndex = 2;
            this.tvUrl.Text = "Website-Url";
            // 
            // pBg
            // 
            this.pBg.Controls.Add(this.tvUrl);
            this.pBg.Controls.Add(this.tvAccountTitle);
            this.pBg.Controls.Add(this.ivLogo);
            this.pBg.Location = new System.Drawing.Point(0, 0);
            this.pBg.Name = "pBg";
            this.pBg.Size = new System.Drawing.Size(419, 70);
            this.pBg.TabIndex = 3;
            this.pBg.MouseLeave += new System.EventHandler(this.pBg_MouseLeave);
            // 
            // ivLogo
            // 
            this.ivLogo.Image = global::AntivirusUi.Properties.Resources.Instagram_logo_2016_svg;
            this.ivLogo.Location = new System.Drawing.Point(15, 16);
            this.ivLogo.Name = "ivLogo";
            this.ivLogo.Size = new System.Drawing.Size(40, 40);
            this.ivLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ivLogo.TabIndex = 0;
            this.ivLogo.TabStop = false;
            this.ivLogo.MouseLeave += new System.EventHandler(this.pBg_MouseLeave);
            // 
            // ListItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.Controls.Add(this.pBg);
            this.Name = "ListItem";
            this.Size = new System.Drawing.Size(442, 70);
            this.pBg.ResumeLayout(false);
            this.pBg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ivLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pBg;
        public System.Windows.Forms.PictureBox ivLogo;
        public System.Windows.Forms.Label tvAccountTitle;
        public System.Windows.Forms.Label tvUrl;
    }
}
