﻿using System;

namespace AntivirusUi
{
    partial class PasswordManagerScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flAccountsList = new System.Windows.Forms.FlowLayoutPanel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnAddAccount = new System.Windows.Forms.Button();
            this.pAccountDetails = new System.Windows.Forms.Panel();
            this.tbAccTitle = new System.Windows.Forms.TextBox();
            this.tbAccUrl = new System.Windows.Forms.TextBox();
            this.tbAccPassword = new System.Windows.Forms.TextBox();
            this.tbAccLogin = new System.Windows.Forms.TextBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ivAccLogo = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.btCheckPass = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pAccountDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ivAccLogo)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // flAccountsList
            // 
            this.flAccountsList.AutoScroll = true;
            this.flAccountsList.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.flAccountsList.Location = new System.Drawing.Point(12, 37);
            this.flAccountsList.Name = "flAccountsList";
            this.flAccountsList.Size = new System.Drawing.Size(492, 471);
            this.flAccountsList.TabIndex = 3;
            this.flAccountsList.Visible = false;
            this.flAccountsList.Paint += new System.Windows.Forms.PaintEventHandler(this.flAccountsList_Paint);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(17, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(341, 21);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Search..";
            // 
            // btnAddAccount
            // 
            this.btnAddAccount.BackColor = System.Drawing.Color.Gray;
            this.btnAddAccount.FlatAppearance.BorderSize = 0;
            this.btnAddAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnAddAccount.ForeColor = System.Drawing.Color.White;
            this.btnAddAccount.Location = new System.Drawing.Point(364, 6);
            this.btnAddAccount.Name = "btnAddAccount";
            this.btnAddAccount.Size = new System.Drawing.Size(53, 22);
            this.btnAddAccount.TabIndex = 1;
            this.btnAddAccount.Text = "+";
            this.btnAddAccount.UseVisualStyleBackColor = false;
            this.btnAddAccount.Click += new System.EventHandler(this.btnAddAccount_Click);
            // 
            // pAccountDetails
            // 
            this.pAccountDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.pAccountDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pAccountDetails.Controls.Add(this.tbAccTitle);
            this.pAccountDetails.Controls.Add(this.tbAccUrl);
            this.pAccountDetails.Controls.Add(this.tbAccPassword);
            this.pAccountDetails.Controls.Add(this.tbAccLogin);
            this.pAccountDetails.Controls.Add(this.btnEdit);
            this.pAccountDetails.Controls.Add(this.btnDelete);
            this.pAccountDetails.Controls.Add(this.label8);
            this.pAccountDetails.Controls.Add(this.label7);
            this.pAccountDetails.Controls.Add(this.label3);
            this.pAccountDetails.Controls.Add(this.label2);
            this.pAccountDetails.Controls.Add(this.label1);
            this.pAccountDetails.Controls.Add(this.ivAccLogo);
            this.pAccountDetails.Location = new System.Drawing.Point(935, -9);
            this.pAccountDetails.Name = "pAccountDetails";
            this.pAccountDetails.Size = new System.Drawing.Size(507, 517);
            this.pAccountDetails.TabIndex = 4;
            this.pAccountDetails.Visible = false;
            this.pAccountDetails.Paint += new System.Windows.Forms.PaintEventHandler(this.pAccountDetails_Paint);
            // 
            // tbAccTitle
            // 
            this.tbAccTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tbAccTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbAccTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.tbAccTitle.ForeColor = System.Drawing.Color.White;
            this.tbAccTitle.Location = new System.Drawing.Point(163, 45);
            this.tbAccTitle.Name = "tbAccTitle";
            this.tbAccTitle.ReadOnly = true;
            this.tbAccTitle.Size = new System.Drawing.Size(198, 31);
            this.tbAccTitle.TabIndex = 18;
            this.tbAccTitle.Text = "Title";
            this.tbAccTitle.TextChanged += new System.EventHandler(this.checkCanApplyDetails);
            // 
            // tbAccUrl
            // 
            this.tbAccUrl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tbAccUrl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbAccUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tbAccUrl.ForeColor = System.Drawing.Color.White;
            this.tbAccUrl.Location = new System.Drawing.Point(163, 218);
            this.tbAccUrl.Name = "tbAccUrl";
            this.tbAccUrl.ReadOnly = true;
            this.tbAccUrl.Size = new System.Drawing.Size(298, 17);
            this.tbAccUrl.TabIndex = 17;
            this.tbAccUrl.Text = "alonkopilov";
            this.tbAccUrl.TextChanged += new System.EventHandler(this.checkCanApplyDetails);
            // 
            // tbAccPassword
            // 
            this.tbAccPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tbAccPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbAccPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tbAccPassword.ForeColor = System.Drawing.Color.White;
            this.tbAccPassword.Location = new System.Drawing.Point(163, 185);
            this.tbAccPassword.Name = "tbAccPassword";
            this.tbAccPassword.ReadOnly = true;
            this.tbAccPassword.Size = new System.Drawing.Size(298, 17);
            this.tbAccPassword.TabIndex = 16;
            this.tbAccPassword.Text = "alonkopilov";
            this.tbAccPassword.TextChanged += new System.EventHandler(this.checkCanApplyDetails);
            // 
            // tbAccLogin
            // 
            this.tbAccLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tbAccLogin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbAccLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tbAccLogin.ForeColor = System.Drawing.Color.White;
            this.tbAccLogin.Location = new System.Drawing.Point(163, 153);
            this.tbAccLogin.Name = "tbAccLogin";
            this.tbAccLogin.ReadOnly = true;
            this.tbAccLogin.Size = new System.Drawing.Size(298, 17);
            this.tbAccLogin.TabIndex = 15;
            this.tbAccLogin.Text = "alonkopilov";
            this.tbAccLogin.TextChanged += new System.EventHandler(this.checkCanApplyDetails);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnEdit.FlatAppearance.BorderSize = 0;
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.ForeColor = System.Drawing.Color.White;
            this.btnEdit.Location = new System.Drawing.Point(367, 28);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(54, 22);
            this.btnEdit.TabIndex = 14;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(427, 28);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(54, 22);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(51, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(488, 18);
            this.label8.TabIndex = 13;
            this.label8.Text = "____________________________________________________________";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(160, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Account Details";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label3.Location = new System.Drawing.Point(51, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Website Url:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(51, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Password:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label1.Location = new System.Drawing.Point(51, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Login:";
            // 
            // ivAccLogo
            // 
            this.ivAccLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ivAccLogo.Image = global::AntivirusUi.Properties.Resources._94e9035a11b4869bec9266f730084bac;
            this.ivAccLogo.Location = new System.Drawing.Point(55, 28);
            this.ivAccLogo.Name = "ivAccLogo";
            this.ivAccLogo.Size = new System.Drawing.Size(85, 85);
            this.ivAccLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ivAccLogo.TabIndex = 3;
            this.ivAccLogo.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.tbPassword);
            this.panel1.Controls.Add(this.btCheckPass);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(934, 520);
            this.panel1.TabIndex = 5;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AntivirusUi.Properties.Resources.key;
            this.pictureBox1.Location = new System.Drawing.Point(407, 108);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(121, 115);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // tbPassword
            // 
            this.tbPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tbPassword.Font = new System.Drawing.Font("Poppins", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPassword.ForeColor = System.Drawing.Color.White;
            this.tbPassword.Location = new System.Drawing.Point(245, 293);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(444, 39);
            this.tbPassword.TabIndex = 19;
            this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbPassword.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // btCheckPass
            // 
            this.btCheckPass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.btCheckPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCheckPass.Font = new System.Drawing.Font("Poppins", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCheckPass.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btCheckPass.Location = new System.Drawing.Point(245, 338);
            this.btCheckPass.Name = "btCheckPass";
            this.btCheckPass.Size = new System.Drawing.Size(444, 35);
            this.btCheckPass.TabIndex = 3;
            this.btCheckPass.Text = "Enter";
            this.btCheckPass.UseVisualStyleBackColor = false;
            this.btCheckPass.Click += new System.EventHandler(this.btCheckPass_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Poppins", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(245, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(451, 42);
            this.label5.TabIndex = 2;
            this.label5.Text = "Please Enter your Master Password:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Window;
            this.label4.Location = new System.Drawing.Point(33, 85);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 0;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // PasswordManagerScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ClientSize = new System.Drawing.Size(934, 520);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pAccountDetails);
            this.Controls.Add(this.btnAddAccount);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.flAccountsList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PasswordManagerScreen";
            this.Text = "PasswordManagerScreen";
            this.pAccountDetails.ResumeLayout(false);
            this.pAccountDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ivAccLogo)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel flAccountsList;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnAddAccount;
        private System.Windows.Forms.Panel pAccountDetails;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox ivAccLogo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox tbAccLogin;
        private System.Windows.Forms.TextBox tbAccUrl;
        private System.Windows.Forms.TextBox tbAccPassword;
        private System.Windows.Forms.TextBox tbAccTitle;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btCheckPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}